//
//  AppDelegate.swift
//  Edulive
//
//  Created by Mac on 2/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import HHTabBarView

var user:UserModel?

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , WXApiDelegate{

    var loginVC : LoginVC!
    public static var hhTabBarView = HHTabBarView.shared
    
    public static let referenceUITabBarController = HHTabBarView.shared.referenceUITabBarController
    
    public static func setupReferenceUITabBarController() {
            
            //Creating a storyboard reference
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            
            //First and Second Tab ViewControllers will be taken from the UIStoryBoard
            //Creating navigation controller for navigation inside the first tab.
            var navigationController1: UINavigationController!
            var navigationController2: UINavigationController!
            var navigationController3: UINavigationController!
            var navigationController4: UINavigationController!
        
            if user!.type == PARAMS.TEACHER{
                 navigationController1 = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "MainTeacher"))
                navigationController2 = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "FilterVC"))
                 navigationController3 = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "FavoriteTeacherVC"))
                 navigationController4 = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "SettingsTeacher"))
            }
            else{
                navigationController1 = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "StudentMainVC"))
                navigationController2 = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "FilterVC"))
                navigationController3 = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "PurchasedVC"))
                navigationController4 = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "SettingVC"))
            }
        
            navigationController1.isNavigationBarHidden = true
            navigationController2.isNavigationBarHidden = true
            navigationController3.isNavigationBarHidden = true
            navigationController4.isNavigationBarHidden = true
            //Update referenced TabbarController with your viewcontrollers
            AppDelegate.referenceUITabBarController.setViewControllers([navigationController1, navigationController2,navigationController3,navigationController4], animated: false)
        }
        
        //3
        public static func setupHHTabBarView() {
            
            //Default & Selected Background Color
            let defaultTabColor = UIColor.white.withAlphaComponent(1.0)
            let selectedTabColor = UIColor.white.withAlphaComponent(1.0)
            let tabFont = UIFont.init(name: "Helvetica-Light", size: 12.0)
            let spacing: CGFloat = 3.0
            
            let icons = [UIImage(named: "home_UNSELECTED")!, UIImage(named: "search_UNSELECTED")!, UIImage(named: "chart_UNSELECTED")!, UIImage(named: "etc_UNSELECTED")!]
            
            let icons_selected = [UIImage(named: "home_SELECTED")!,UIImage(named: "search_SELECTED")!, UIImage(named: "chart_SELECTED")!, UIImage(named: "etc_SELECTED")!]
            
            
            var tabs = [HHTabButton]()
            
            for index in 0...3 {
                let tab = HHTabButton(withTitle: nil, tabImage: icons[index], index: index)
                tab.setImage(icons_selected[index], for: .selected)
                
                tab.titleLabel?.font = tabFont
                tab.setHHTabBackgroundColor(color: defaultTabColor, forState: .normal)
                tab.setHHTabBackgroundColor(color: selectedTabColor, forState: .selected)
                tab.imageToTitleSpacing = spacing
                tab.imageVerticalAlignment = .top
                tab.imageHorizontalAlignment = .center
                
                tabs.append(tab)
            }
            
            //Set HHTabBarView position.
            hhTabBarView.tabBarViewPosition = .bottom
            
            //Set this value according to your UI requirements.
            hhTabBarView.tabBarViewTopPositionValue = 128

            //Set Default Index for HHTabBarView.
            hhTabBarView.tabBarTabs = tabs
            
            // To modify badge label.
            // Note: You should only modify badgeLabel after assigning tabs array.
            // Example:
            //t1.badgeLabel?.backgroundColor = .white
            //t1.badgeLabel?.textColor = selectedTabColor
            //t1.badgeLabel?.font = UIFont.boldSystemFont(ofSize: 20.0)
            
            //Handle Tab Change Event
            hhTabBarView.defaultIndex = 0
            
            //Show Animation on Switching Tabs
            hhTabBarView.tabChangeAnimationType = .shake
            
            //Handle Tab Changes
            hhTabBarView.onTabTapped = { (tabIndex, isSameTab, controller) in
                if isSameTab {
                    if let navcon = controller as? UINavigationController {
                        navcon.popToRootViewController(animated: true)
                    } else if let vc = controller as? UIViewController {
                        vc.navigationController?.popToRootViewController(animated: true)
                    }
                }
                print("Selected Tab Index:\(tabIndex)")
            }
        }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        user = UserModel()
        user?.loadUserInfo()
        // WeChat: replace with your AppID
        WXApi.registerApp(Constants.WECHAT_ID)
        
        return true
    }
    
     @available(iOS 9.0, *)
       func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
           return WXApi.handleOpen(url, delegate: self)
    }
    
    func test (){
        NotificationCenter.default.post(Notification(name: .wechat_success, object: nil,userInfo: ["test":"test"] ))
    }
    
    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    @available(iOS 13.0, *)
    @available(iOS 13.0, *)
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    }
}

extension Notification.Name {
    static let wechat_success = Notification.Name("wechat_success")
    static let wechat_fail = Notification.Name("wechat_fail")
    static let wechat_pay_success = Notification.Name("wechat_pay_success")
    static let wechat_pay_success1 = Notification.Name("wechat_pay_success1")
    static let wechat_pay_success2 = Notification.Name("wechat_pay_success2")
    static let wechat_success1 = Notification.Name("wechat_success1")
    static let wechat_fail1 = Notification.Name("wechat_fail1")
    static let wechat_pay_fail = Notification.Name("wechat_pay_fail")
    static let wechat_pay_fail1 = Notification.Name("wechat_pay_fail1")
    static let wechat_pay_fail2 = Notification.Name("wechat_pay_fail2")
}
struct WeChat {
    static let appID = "wx70c1840e6b058211"
    static let appSecret = ""
    static let universalLink = "https://eduintel.us/"
}

