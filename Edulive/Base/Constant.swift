
import Foundation
//import Firebase
import UIKit

//*****************************************************************//
                        //Global Variable//
//*****************************************************************//

var loginNav : UINavigationController!
var mainNav : UINavigationController!
var loginVC: LoginVC!
var alertController : UIAlertController? = nil
let durationOfAnimationInSecond = 1.0
var categoriType = ""
var subIndex = 0
var global_amount = 100
var recommendedTeacherDatasource = [TeacherModel]()
var contentDataSource = [ContentModel]()
var favoriteDataSource = [ContentModel]()
var subDataSource = [CollectionModel]()
var teacherLessonDataSource = [LessonModel]()
var userDataSource = [CollectionModel]()
var totalUserDataSource = [TotalUserModel]()

var arr_subjectId = [String]()
var contentIndex = 0 // only for create lesson from contentList to the create lesson.
var allcontentDataSource = [ContentModel]() // for the all lessons for the student's side
var student_teacherlessonsDataSource = [InstructorModel]() // for the teacher's all lessons
var subjectCaption = ""
var fileURL = ""
var descriptionText = ""
// payment variable
var str_payprice = ""
var str_payContentId = ""
var str_paycontentName = ""
var str_paidTeacherId = ""
var str_paidTeacherName = ""
var str_paidContentTitle = ""

var contentTitle = ""
var descriptionLast = ""
var amountLast = ""
var idLast = ""
var confRoomName = ""
var wechatRequest = 0
var edit = false
var editCategory = user!.category

struct Constants {
    static let WECHAT_ID = "wx70c1840e6b058211"
    static let WECHAT_SECRET = "28d51a00bc2cbff39ea167db2b4d13ef"
    static let WECHAT_MERCHANT_NUMBER = "1578086161"
    static let WECHAT_NOTIFY_URL = "https://eduintel.us/WxPay"
    static let WECHAT_TRADE_TYPE = "APP"
    static let WECHAT_API_KEY = "Eduintel1edulivechinateacherstud"
    
    static let SAVE_ROOT_PATH = "psj"
    static let TOTAL_CALORIE = "total_cal"
    static let EMAIL = "email"
    static let REMAINING_CALORIE = "remaining_cal"
    static let TOTAL_BREAKFAST_CALORIE = "break_cal"
    static let TOTAL_FIRST_SNACK_CALORIE = "first_cal"
    static let TOTAL_LUNCH_CALORIE = "lunch_cal"
    static let TOTAL_SECOND_SNACK_CALORIE = "second_cal"
    static let TOTAL_DINNER_CALORIE = "dinner_cal"
    static let TOTAL_THIRD_SNACK_CALORIE = "third_cal"
    static let DAILY_SCORE = "daily_score"
    static let DAILY_GRADE = "daily_grade"
    static let OLD_VALUE = "old_value"
    static let BREAKFAST = "1"
    static let FIRTSNACK = "2"
    static let LUNCH = "3"
    static let SECONDSNACK = "4"
    static let DINNER = "5"
    static let THIRDSNACK = "6"
    static let LASTTIMEDATE = "lasttimedate"
    static let LOGOUT = "logout"
    static let BACKGROUND = "background"
}
