//
//  R.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/18/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import Foundation
import Foundation

struct R {
    
    struct string {
        
        static let SETTINGS = "SETTINGS"
        static let BALCK_LIST = "BLACKLIST"
        static let ADD_OPTIONS = "ADD OPTIONS"
        static let STATUS = "STATUS"
        static let MESSAGES = "MESSAGES"
        
        
        static let ADD_FRIENDS = "Add friends"
        static let NOTIFICATIONS = "Notifications"
        static let LANGUAGE = "Language"
        static let SECRITY = "Security"
        static let SUPPORT = "Support"
        static let INFO = "Info"
        static let BLACK_LIST = "Black List"
        static let SHARE = "Share"
        static let LOGOUT = "Logout"
        static let ENGLISH = "English"
        static let TERMS_OF_USE = "Terms of Use"
        static let PRIVACY_POLICY = "privacy Policy"
        static let ABOUT_US = "About us"
        static let RATE_US = "Rate Us"
        
    }
}
