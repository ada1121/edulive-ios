//
//  array_extension.swift
//  Edulive
//
//  Created by Mac on 3/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()

        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }

        return result
    }
}
