//
//  PDFViewer.swift
//  Edulive
//
//  Created by Mac on 2/20/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import WebKit

class PDFViewer: BaseVC1 {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var pdfName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pdfName.text = contentTitle
        let str = fileURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: str)
        if url != nil{
            webView.load(URLRequest(url: url!))
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        pdfName.text = contentTitle
    }
    @IBAction func gotoBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

