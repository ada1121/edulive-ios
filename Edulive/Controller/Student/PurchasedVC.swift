//
//  PurchasedVC.swift
//  Edulive
//
//  Created by Mac on 3/8/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import SwipeCellKit
import SwiftyJSON
import SwiftyUserDefaults
import Alamofire
import SwiftyXMLParser
import CryptoKit
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG

class PurchasedVC: BaseVC1 {

    @IBOutlet weak var ui_collectionView: UICollectionView!
    var purchasedDataSource = [ContentModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        self.getPurchasedLessons()
    }
    
    func getPurchasedLessons() {
        let toast = NSLocalizedString("Connecting... \n Just a moment!", comment: "")
        self.showProgressSet_withMessage(toast, msgOn: false, styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
        self.purchasedDataSource.removeAll()
        ApiManager.getPurchasedLessons(user_id: user!.id){
            (isSuccess, data) in
            self.hideProgress()
            if isSuccess{
                let purchased_lessons = JSON(data as Any)
                if purchased_lessons.count == 0 {
                    self.alertDisplay(alertController: self.alertMake("There is no any purchased lessons yet! \n Please purchase for lessons!"))
                }
                
                var count = 0
                for one in purchased_lessons{
                    let jsoncontenlist = JSON(one.1)
                    self.purchasedDataSource.append(ContentModel(jsoncontenlist))
                    
                    print(jsoncontenlist.count)
                    
                    count += 1
                    if count == purchased_lessons.count{
                        self.ui_collectionView.reloadData()
                    }
                }
            }
        }
    }
    
    @IBAction func favoriteBtnClicked(_ sender: Any) {
        let senderBtn : UIButton = sender as! UIButton
        let contentIndex = senderBtn.tag
        var status = ""
        if self.purchasedDataSource[contentIndex].favorite=="yes"{
             status = "no"
        }
        else{
             status = "yes"
        }
        
        let content_id = self.purchasedDataSource[contentIndex].id
        let toast = NSLocalizedString("Connecting... \n Just a moment!", comment: "")
        self.showProgressSet_withMessage(toast, msgOn: false, styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
        ApiManager.favorite(user_id: user!.id, content_id: content_id, status: status){(isSuccess, data) in
            self.hideProgress()
            if isSuccess{
                self.getPurchasedLessons()
                self.ui_collectionView.reloadData()
            }
            else{
                self.alertDisplay(alertController: self.alertMake("Network Error!"))
                self.ui_collectionView.reloadData()
            }
        }
    }
}

extension PurchasedVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.purchasedDataSource.count
    }
    
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
   
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavoriteCell", for: indexPath) as! FavoriteCell
        cell.entity = self.purchasedDataSource[indexPath.row]
        let pathrow = indexPath.row
        cell.favoriteBtn.tag = pathrow
        //cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        confRoomName = ""
        confRoomName = self.purchasedDataSource[indexPath.row].id
        self.gotoVC("BroadCastNav")
    }
}

extension PurchasedVC : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let w = collectionView.frame.size.width / 2.1
        let h = w
        return CGSize(width: w, height: h)
        
    }
}

extension PurchasedVC : SwipeCollectionViewCellDelegate {

    func collectionView(_ collectionView: UICollectionView, editActionsForItemAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else {
                   return nil
                }
                
                let videoAction = SwipeAction(style: .default, title: nil) { (action, indexPath) in
                    confRoomName = ""
                    confRoomName = self.purchasedDataSource[indexPath.row].id
                    self.gotoVC("BroadCastNav")
                }

                let downLoadAction = SwipeAction(style: .default, title: nil) { (action, indexPath) in
                    contentTitle = self.purchasedDataSource[indexPath.row].title
                    fileURL = self.purchasedDataSource[indexPath.row].file_url
                    if fileURL != ""{
                        collectionView.reloadData()
                        self.gotoNavPresent1("PDFViewer")
                    }
                    else{
                        self.alertDisplay(alertController: self.alertMake("There is no attached pdf file!"))
                    }
                }
        
                videoAction.transitionDelegate = ScaleTransition(duration: 0.15, initialScale: 0.7, threshold: 0.8)
                downLoadAction.transitionDelegate = ScaleTransition(duration: 0.15, initialScale: 0.7, threshold: 0.8)

                       // customize the action appearance
                videoAction.image = UIImage(named: "videoconferencing")
                downLoadAction.image = UIImage(named: "download")
                videoAction.backgroundColor = UIColor.init(named: "textColor")
                downLoadAction.backgroundColor = UIColor.init(named: "textColor")
                
                videoAction.title = NSLocalizedString("Conference", comment: "")
                downLoadAction.title = NSLocalizedString("Download", comment: "")
                videoAction.textColor = UIColor.lightGray
                downLoadAction.textColor = UIColor.lightGray

                return [downLoadAction,videoAction]

    }
}

