
//  ContentListVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/25/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import SwipeCellKit
import CryptoKit
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG
import Alamofire
import SwiftyXMLParser
import SwiftyJSON

class ContentListVC: BaseVC1 {
    
    @IBOutlet weak var topcaption: UILabel!
    @IBOutlet weak var ui_collectionView: UICollectionView!
    var str_payContentId1 = ""
    var str_payprice1 = ""
    var str_paycontentName1 = ""
    var str_paidTeacherId1 = ""
    var str_paidTeacherName1 = ""
    var str_paidContentTitle1 = ""
    
    var wechatmodel : WechatProductModel?
    var req_prepay_id = ""
    var req_nonce_str = ""
    var req_sign = ""
    var req_timeStamp : UInt32!
    var contentListDataSource = [ContentModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        confRoomName = ""
        //editInit()
        descriptionText = ""
        topcaption.text = subjectCaption
        
        // payment variable init
        str_payContentId1 = ""
        str_payprice1 = ""
        str_paycontentName1 = ""
        str_paidTeacherId1 = ""
        str_paidTeacherName1 = ""
        str_paidContentTitle1 = ""
        NotificationCenter.default.addObserver(self, selector: #selector(onWechatPaySuccess1(_:)), name: Notification.Name.wechat_pay_success1, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onWechatPayFail1(_:)), name: Notification.Name.wechat_pay_fail1, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        self.getUpdatedContents()
    }
    @objc func onWechatPaySuccess1(_ notification : Notification) {
        print(notification)
        self.progShowSuccess(true, msg: "Pay Success!")
        
        confRoomName = ""
        //confRoomName = str_payContentId
        str_payContentId = ""
        str_payprice = ""
        str_paycontentName = ""
        str_paidTeacherId = ""
        str_paidTeacherName = ""
        str_paidContentTitle1 = ""
        //self.gotoVC("BroadCastNav")
        
    }
    
    @objc func onWechatPayFail1(_ notifcaion : Notification) {
        showToast(NSLocalizedString("error", comment: ""))
        //self.progShowError(true, msg: "Error!")
    }
    
//    func editInit()  {
//
//        searchTextfield.addPadding(.left(16))
//        searchTextfield.attributedPlaceholder = NSAttributedString(string: "Search lesson",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
//    }
    
    // for the click event for the each items
    
    @IBAction func titleBtnClicked(_ sender: Any) {
        //if user!.type == "student"{
            
        let senderBtn : UIButton = sender as! UIButton
        let contentIndex = senderBtn.tag
        descriptionText = self.contentListDataSource[contentIndex].description
        str_payprice = self.contentListDataSource[contentIndex].amount
        str_payContentId = self.contentListDataSource[contentIndex].id
        str_paycontentName = self.contentListDataSource[contentIndex].title
        str_paidTeacherId = self.contentListDataSource[contentIndex].user_id
        str_paidTeacherName = self.contentListDataSource[contentIndex].username
        str_paidContentTitle = self.contentListDataSource[contentIndex].title
        self.gotoNavPresent1("Description")
        /*}
       else{
           return
       }*/
    }
    
    @IBAction func avartar_teacherNameBtnClicked(_ sender: Any) {
        
        let senderBtn : UIButton = sender as! UIButton
        
        let contentIndex = senderBtn.tag
        
        if contentListDataSource.count != 0 && contentListDataSource.count > contentIndex{
            let teacher_id = self.contentListDataSource[contentIndex].user_id.toInt()!
            self.showProgress()
            ApiManager.getMyAllContents(user_id: teacher_id){
                (isSuccess, data) in
                self.hideProgress()
                if isSuccess{
                 
                    self.hideProgress()
                    let contentList = JSON(data as Any)
                    var count = 0
                     student_teacherlessonsDataSource.removeAll()
                     for one in contentList{
                         let jsoncontenlist = JSON(one.1)
                         student_teacherlessonsDataSource.append(InstructorModel(jsoncontenlist))
                         count += 1
                         if count == contentList.count{
                             self.gotoNavPresent1("InstructorVC")
                         }
                     }
                }
            }
        }
        else{
            return
        }
       
    }
    
    @IBAction func favoriteBtnClicked(_ sender: Any) {
        
        //if user!.type == "student"{
            let senderBtn : UIButton = sender as! UIButton
            let contentIndex = senderBtn.tag
            print(contentIndex)
            
            if contentListDataSource.count != 0 && contentListDataSource.count > contentIndex{
                let content_id = self.contentListDataSource[contentIndex].id
                    self.showProgress()
                    
                    if self.contentListDataSource[contentIndex].favorite == "yes"{
                        ApiManager.favorite(user_id: user!.id, content_id: content_id, status: "no"){
                            (isSuccess, data) in
                            self.hideProgress()
                            if isSuccess{
                                self.hideProgress()
                                self.getUpdatedContents()
                            }
                        }
                    }
                    else{
                        ApiManager.favorite(user_id: user!.id, content_id: content_id, status: "yes"){
                            (isSuccess, data) in
                            self.hideProgress()
                            if isSuccess{
                                self.hideProgress()
                                self.getUpdatedContents()
                            }
                        }
                    }
                }
                else{
                    return
                }
            //}
//        else{
//            return
//        }
            
    }
    
    func getUpdatedContents()  {
        //var amount = 0
        self.contentListDataSource.removeAll()
        
        ApiManager.getContent(categorie_id: subIndex, amount: 100,user_id: user!.id){
            (isSuccess, data) in
            if isSuccess{
                self.hideProgress()
                let contentList = JSON(data as Any)
                var count = 0
                for one in contentList{
                    let jsoncontenlist = JSON(one.1)
                    self.contentListDataSource.append(ContentModel(jsoncontenlist))
                    count += 1
                    if count == contentList.count{
                        self.ui_collectionView.reloadData()
                    }
                }
            }
        }
    }
    
    // for the back button click event
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ContentListVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.contentListDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContentCell", for: indexPath) as! ContentCell
        
        cell.entity = self.contentListDataSource[indexPath.row]
        let pathrow = indexPath.row
        cell.titleBtn.tag = pathrow
        cell.teacherBtn.tag = pathrow
        cell.avartarBtn.tag = pathrow
        cell.favoriteBtn.tag = pathrow
        if user!.type == "student"{
            cell.delegate = self
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}

extension ContentListVC: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let h: CGFloat = 100
        let w = collectionView.frame.size.width
        
        return CGSize(width: w, height: h)
        
    }
    
}

extension ContentListVC: SwipeCollectionViewCellDelegate {
    func collectionView(_ collectionView: UICollectionView, editActionsForItemAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        guard orientation == .right else {
           return nil
        }
        let str_contentIndex_contentList = indexPath.row
        self.str_payprice1 = self.contentListDataSource[str_contentIndex_contentList].amount
        self.str_payContentId1 = self.contentListDataSource[str_contentIndex_contentList].id
        self.str_paycontentName1 = self.contentListDataSource[str_contentIndex_contentList].title
        self.str_paidTeacherId1 = self.contentListDataSource[str_contentIndex_contentList].user_id
        self.str_paidTeacherName1 = self.contentListDataSource[str_contentIndex_contentList].username
        self.str_paidContentTitle1 = self.contentListDataSource[str_contentIndex_contentList].title

        let videoAction = SwipeAction(style: .default, title: nil) { (action, indexPath) in
//            confRoomName = contentDataSource[indexPath.row].id
//            self.gotoVC("BroadCastNav")
            if self.str_payprice1 != "0" && self.str_payprice1 != ""{
                self.showProgress()
                ApiManager.getPaid(user_id: user!.id, content_id: self.str_payContentId1){
                    (isSuccess, data) in
                    self.hideProgress()
                    if isSuccess{
                        confRoomName = str_payContentId
                        self.gotoVC("BroadCastNav")
                    }
                    else{
                        let stringParams = self.toXml(self.makeWechatModel().toDictionary())
                        print(stringParams)
                        
                        let url = URL(string:FIRST_REQUEST)
                        var xmlRequest = URLRequest(url: url!)
                        xmlRequest.httpBody = stringParams.data(using: String.Encoding.utf8, allowLossyConversion: true)
                        xmlRequest.httpMethod = "POST"
                        xmlRequest.addValue("application/xml", forHTTPHeaderField: "Content-Type")

                        
                        //let request = Alamofire.request(xmlRequest)
                        //debugPrint("**********",request)
                        Alamofire.request(xmlRequest)
                            .responseData { (response) in
                                let stringResponse: String = (String(data: response.data!, encoding: String.Encoding.utf8) as String?)!
                                debugPrint(stringResponse)
                                self.gotoXmlparser(stringResponse)
                        }
                    }
                }
            }
            else{
                confRoomName = str_payContentId
                self.gotoVC("BroadCastNav")
            }
        }

        let downLoadAction = SwipeAction(style: .default, title: nil) { (action, indexPath) in
            contentTitle = self.contentListDataSource[indexPath.row].title
            fileURL = self.contentListDataSource[indexPath.row].file_url
            
            if self.str_payprice1 != "0" && self.str_payprice1 != ""{
                self.showProgress()
                ApiManager.getPaid(user_id: user!.id, content_id: self.str_payContentId1){
                    (isSuccess, data) in
                    self.hideProgress()
                    if isSuccess{
                        if fileURL != ""{
                            collectionView.reloadData()
                            self.gotoNavPresent1("PDFViewer")
                        }
                        else{
                            return
                        }
                    }
                    else{
                        let stringParams = self.toXml(self.makeWechatModel().toDictionary())
                        print(stringParams)
                        
                        let url = URL(string:FIRST_REQUEST)
                        var xmlRequest = URLRequest(url: url!)
                        xmlRequest.httpBody = stringParams.data(using: String.Encoding.utf8, allowLossyConversion: true)
                        xmlRequest.httpMethod = "POST"
                        xmlRequest.addValue("application/xml", forHTTPHeaderField: "Content-Type")

                        
                        //let request = Alamofire.request(xmlRequest)
                        //debugPrint("**********",request)
                        Alamofire.request(xmlRequest)
                            .responseData { (response) in
                                let stringResponse: String = (String(data: response.data!, encoding: String.Encoding.utf8) as String?)!
                                debugPrint(stringResponse)
                                self.gotoXmlparser(stringResponse)
                        }
                    }
                }
            }
            else{
                if fileURL != ""{
                    collectionView.reloadData()
                    self.gotoNavPresent1("PDFViewer")
                }
                else{
                    return
                }
            }
        }
               // customize swipe action
        videoAction.transitionDelegate = ScaleTransition(duration: 0.15, initialScale: 0.7, threshold: 0.8)
        downLoadAction.transitionDelegate = ScaleTransition(duration: 0.15, initialScale: 0.7, threshold: 0.8)

               // customize the action appearance
        videoAction.image = UIImage(named: "videoconferencing")
        downLoadAction.image = UIImage(named: "download")
        videoAction.backgroundColor = UIColor.init(named: "textColor")
        downLoadAction.backgroundColor = UIColor.init(named: "textColor")
        
        videoAction.title = NSLocalizedString("Conference", comment: "")
        downLoadAction.title = NSLocalizedString("Download", comment: "")
        videoAction.textColor = UIColor.lightGray
        downLoadAction.textColor = UIColor.lightGray

        return [downLoadAction,videoAction]

    }
}

extension ContentListVC{
    
    func sendPayReq() {
        
        let request = PayReq()
        request.partnerId = Constants.WECHAT_MERCHANT_NUMBER
        request.prepayId = req_prepay_id
        request.nonceStr = req_nonce_str
        request.timeStamp = req_timeStamp
        request.package = "Sign=WXPay"
        request.sign = req_sign
        wechatRequest = 4
        WXApi.send(request)
        
    }
    
    func gotoXmlparser(_ xmlString : String) {
        
        let xml = try! XML.parse(xmlString)
        if let perpay_id = xml["xml", "prepay_id"].text {
            req_prepay_id = perpay_id
        }
        if let nonce_str = xml["xml", "nonce_str"].text {
            req_nonce_str = nonce_str
        }
        self.makeReq_sign()
    }
    
    func makeReq_sign() {
        
        var returnedString = ""
        req_timeStamp = UInt32(NSDate().timeIntervalSince1970)
        returnedString = "appid=" + Constants.WECHAT_ID  + "&" + "noncestr=" + req_nonce_str + "&" + "package=" + "Sign=WXPay" + "&" + "partnerid=" + Constants.WECHAT_MERCHANT_NUMBER  + "&" + "prepayid=" + req_prepay_id + "&" + "timestamp=" + "\(req_timeStamp ?? 0)" + "&" + "key=" + Constants.WECHAT_API_KEY
        req_sign = "\(MD5(string: returnedString).map { String(format: "%02hhx", $0) }.joined())".uppercased()
        self.sendPayReq()
    }
    
    func toXml(_ dict: Dictionary<AnyHashable, Any>) -> String {
        
        var str = "<xml>"
        for (key, value) in dict {
            str.append("<" + "\(key)" + ">")
            str.append("\(value)")
            str.append("</" + "\(key)" + ">")
        }
        str.append("</xml>")
        print(str)
        return str
    }
        
    func makeWechatModel() -> WechatProductModel {
        
        let d = randomString(length: 5)
        let nonce_str = "\(MD5(string: d).map { String(format: "%02hhx", $0) }.joined())"
        let out_trade_no = Int(NSDate().timeIntervalSince1970) * 1000
        let user_id = user?.id
        let username = user?.username
        let attached = "\(user_id ?? 0)" + "_" + username! + "_" + str_payContentId1 + "_" + str_paycontentName1 + "_" + str_paidTeacherId1 + "_" + str_paidTeacherName1 + "_" + str_payprice1// user_id,user_name,content_id, content_name, teacher_id, teacher_name, amount
        let body = "12months"
        let paidamout = str_payprice1.toInt()!
        wechatmodel = WechatProductModel(appid: Constants.WECHAT_ID, mch_id: Constants.WECHAT_MERCHANT_NUMBER, nonce_str: "\(nonce_str)", sign:Constants.WECHAT_API_KEY, body: body, out_trade_no: "\(out_trade_no)", total_fee: paidamout * 100, spbill_create_ip: "127.0.0.1", notify_url: Constants.WECHAT_NOTIFY_URL, trade_type: Constants.WECHAT_TRADE_TYPE, attach: attached)
        let sign_original = (wechatmodel?.toString())! + "key=" + Constants.WECHAT_API_KEY
        let sign = "\(MD5(string: sign_original).map { String(format: "%02hhx", $0) }.joined())".uppercased()
        wechatmodel?.sign = sign
        return wechatmodel!
    }
        
    func MD5(string: String) -> Data {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: length)

        _ = digestData.withUnsafeMutableBytes { digestBytes -> UInt8 in
            messageData.withUnsafeBytes { messageBytes -> UInt8 in
                if let messageBytesBaseAddress = messageBytes.baseAddress, let digestBytesBlindMemory = digestBytes.bindMemory(to: UInt8.self).baseAddress {
                    let messageLength = CC_LONG(messageData.count)
                    CC_MD5(messageBytesBaseAddress, messageLength, digestBytesBlindMemory)
                }
                return 0
            }
        }
        return digestData
    }
}
