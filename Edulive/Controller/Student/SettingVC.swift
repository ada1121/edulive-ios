//
//  SettingVC.swift
//  Edulive
//
//  Created by Mac on 2/26/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class SettingVC: BaseVC1 , UITableViewDelegate , UITableViewDataSource {

    var settingDatasource = [SettingModel]()
    @IBOutlet weak var imv_profile: UIImageView!
    @IBOutlet weak var lbl_name : UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: user!.photo ?? "") // must be chaged
        imv_profile.kf.setImage(with: url,placeholder: UIImage(named: "account_photo"))
        lbl_name.text = user!.username
        initDatasource()
        // Do any additional setup after loading the view.
    }
    
    func initDatasource() {
        let favorite_course = NSLocalizedString("Favorite Courses", comment: "")
        let archieves = NSLocalizedString("Achievents", comment: "")
        let editlogin_detail = NSLocalizedString("Edit Login Details", comment: "")
        let privacy = NSLocalizedString("Privacy Policy and Terms", comment: "")
        let logout = NSLocalizedString("Log Out", comment: "")
        
        let settingOption = [favorite_course,archieves,editlogin_detail,privacy,logout ]
        for i in 0 ..< settingOption.count{
            let one = SettingModel (settingOption[i])
            settingDatasource.append(one)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return settingDatasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
        cell.entity = settingDatasource[indexPath.row]
        let indextValue = indexPath.row
        
        if(indextValue == 4)
        {
            cell.rightArrow.isHidden = true
        }
        else{
            cell.rightArrow.isHidden = false
        }
        
        if indextValue == 4 {
            cell.setting_lbl.textColor = UIColor.init(named: "dotcolor")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let aa = indexPath.row
        switch  aa {
            
        case 0 :
            self.gotoNavPresent1("FavoriteStudentVC")
            break
        case 1 :
            self.gotoNavPresent1("AchievementVC")
            break
        case 2 :
            self.gotoNavPresent1("editProfileVC")
            break
        case 3 :
            self.gotoNavPresent1("TermsVC")
            break
        case 4 :
        self.logOut()
        break

        default :
            print(aa)
        }
    }
    
    func logOut(){
        
         let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
         loginVC = (storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC)
         loginNav = NavigationController(rootViewController: loginVC)
         loginNav.modalPresentationStyle = .fullScreen
         self.present(loginNav, animated: true, completion: nil)
         loginNav.isNavigationBarHidden = true
         UserDefault.setBool(key: Constants.LOGOUT, value: true)
         UserDefault.setBool(key: PARAMS.SOCIAL_LOGIN, value: false)
    }
}

