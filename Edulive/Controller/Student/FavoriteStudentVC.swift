//
//  FavoriteStudentVC.swift
//  Edulive
//
//  Created by Mac on 2/26/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import SwipeCellKit
import SwiftyJSON
import SwiftyUserDefaults
import Alamofire
import SwiftyXMLParser
import CryptoKit
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG

class FavoriteStudentVC: BaseVC1 {

    @IBOutlet weak var ui_collectionView: UICollectionView!
    // payment variables
    
    var str_payContentId1 = ""
    var str_payprice1 = ""
    var str_paycontentName1 = ""
    var str_paidTeacherId1 = ""
    var str_paidTeacherName1 = ""
    
    var wechatmodel : WechatProductModel?
    var req_prepay_id = ""
    var req_nonce_str = ""
    var req_sign = ""
    var req_timeStamp : UInt32!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initPaymentRefer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        self.getFavoritesLessons()
    }
    
    override func viewDidLayoutSubviews() {
        //self.ui_collectionView.reloadData()
    }
    
    func initPaymentRefer()  {
        str_payContentId = ""
        str_payprice = ""
        str_paycontentName = ""
        str_paidTeacherId = ""
        str_paidTeacherName = ""
    }
    func getFavoritesLessons() {
        let toast = NSLocalizedString("Connecting... \n Just a moment!", comment: "")
        self.showProgressSet_withMessage(toast, msgOn: false, styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
        favoriteDataSource.removeAll()
        ApiManager.getcontentFavorites(user_id: user!.id){
            (isSuccess, data) in
            self.hideProgress()
            if isSuccess{
                let favoriteList = JSON(data as Any)
                if favoriteList.count == 0 {
                    self.alertDisplay(alertController: self.alertMake("There is no any favorite lessons yet! \n Please set favorite lessons!"))
                }
                
                var count = 0
                for one in favoriteList{
                    let jsoncontenlist = JSON(one.1)
                    favoriteDataSource.append(ContentModel(jsoncontenlist))
                    
                    print(jsoncontenlist.count)
                    
                    count += 1
                    if count == favoriteList.count{
                        self.ui_collectionView.reloadData()
                    }
                }
            }
        }
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func favoriteBtnClicked(_ sender: Any) {
        let senderBtn : UIButton = sender as! UIButton
        let contentIndex = senderBtn.tag
        let content_id = favoriteDataSource[contentIndex].id
        let toast = NSLocalizedString("Connecting... \n Just a moment!", comment: "")
        self.showProgressSet_withMessage(toast, msgOn: false, styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
        ApiManager.favorite(user_id: user!.id, content_id: content_id, status: "no"){(isSuccess, data) in
            self.hideProgress()
            if isSuccess{
                favoriteDataSource.remove(at: contentIndex)
                self.ui_collectionView.reloadData()
                self.alertDisplay(alertController: self.alertMake("Set Unfavorite"))
            }
            else{
                self.alertDisplay(alertController: self.alertMake("Network Error!"))
                self.ui_collectionView.reloadData()
            }
        }
    }
}

extension FavoriteStudentVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return favoriteDataSource.count
    }
    
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
   
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavoriteCell", for: indexPath) as! FavoriteCell
        cell.entity = favoriteDataSource[indexPath.row]
        let pathrow = indexPath.row
        cell.favoriteBtn.tag = pathrow
//        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let pathrow = indexPath.row
        descriptionText = favoriteDataSource[pathrow].description
        str_payprice = favoriteDataSource[pathrow].amount
        str_payContentId = favoriteDataSource[pathrow].id
        str_paycontentName = favoriteDataSource[pathrow].title
        str_paidTeacherId = favoriteDataSource[pathrow].user_id
        str_paidTeacherName = favoriteDataSource[pathrow].username
        str_paidContentTitle = favoriteDataSource[pathrow].title
        self.gotoNavPresent1("Description")
    }
}

extension FavoriteStudentVC: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let w = collectionView.frame.size.width / 2.1
        let h = w
        return CGSize(width: w, height: h)
        
    }
}

//extension FavoriteStudentVC : SwipeCollectionViewCellDelegate {
//
//    func collectionView(_ collectionView: UICollectionView, editActionsForItemAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
//        guard orientation == .right else {
//           return nil
//        }
//        //let requestId = datasource[indexPath.row].requestFriendId
//
//        let removeAction = SwipeAction(style: .default, title: nil) { (action, indexPath) in
//            let content_id = favoriteDataSource[indexPath.row].id
//            self.showProgress()
//            ApiManager.favorite(user_id: user!.id, content_id: content_id, status: "no"){(isSuccess, data) in
//                self.hideProgress()
//                if isSuccess{
//                    favoriteDataSource.remove(at: indexPath.row)
//                    self.ui_collectionView.reloadData()
//                    self.alertDisplay(alertController: self.alertMake("Removing Favorite"))
//                }
//                else{
//                    self.alertDisplay(alertController: self.alertMake("Network Error!"))
//                    self.ui_collectionView.reloadData()
//                }
//            }
//        }
//
//        removeAction.transitionDelegate = ScaleTransition(duration: 0.15, initialScale: 0.7, threshold: 0.8)
//
//
//        removeAction.image = UIImage(named: "delete")
//
//        removeAction.backgroundColor = UIColor.darkGray
//
//        removeAction.title = "Remove"
//
//        removeAction.textColor = UIColor.lightGray
//
//        return [removeAction]
//
//    }
//}

