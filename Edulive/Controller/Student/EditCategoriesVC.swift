//
//  EditCategoriesVC.swift
//  Edulive
//
//  Created by Mac on 3/22/20.
//  Copyright © 2020 Mac. All rights reserved.
//


import UIKit

class EditCategoriesVC: BaseVC1{
    
    @IBOutlet weak var sampleTable: UITableView!
    var sampleArray = [String]()
    var cellHeight = 50
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getSampleArray()
    }
    
    func getSampleArray() {
        sampleArray.removeAll()
        for one in subDataSource{
            sampleArray.append(one.subName)
        }
    }
    @IBAction func btnClicked(_ sender: Any) {
        dismiss(animated: true)
    }
}

extension EditCategoriesVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sampleArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SampleCell.self)) as! SampleCell
        cell.sampleMeal.text = sampleArray[indexPath.row]
        cellHeight = Int(cell.bounds.size.height)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let teacherCategories = user!.category
        var categoriArr = [String]()
        categoriArr = teacherCategories?.components(separatedBy: ",") ?? [""]
        var status = true
        for one in categoriArr{
            if one == sampleArray[indexPath.row]{
                status = false
            }
        }
        
        if status{
            var category = user!.category
            category = category! + "," + sampleArray[indexPath.row]
            editCategory = category!
            user!.category = category
            user!.saveUserInfo()
        }
        
        self.navigationController?.popViewController(animated: false)
        //dismiss(animated: true)
    }
}



