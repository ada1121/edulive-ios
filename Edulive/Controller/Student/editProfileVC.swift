//
//  editProfileVC.swift
//  Edulive
//
//  Created by Mac on 3/7/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import UIKit
import SwiftValidator
import IQKeyboardManagerSwift
import SwiftyJSON
import SwiftyUserDefaults
import GDCheckbox
import KRProgressHUD
import KRActivityIndicatorView
import Alamofire

class editProfileVC : BaseVC1, ValidationDelegate, UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    @IBOutlet weak var lbl_catName: UILabel!
    @IBOutlet weak var uiv_categories: UIView!
    @IBOutlet weak var edtName: UITextField!
    @IBOutlet weak var edtEmail: UITextField!
    @IBOutlet weak var edtphoneNumber: UITextField!
    @IBOutlet weak var edtPwd: UITextField!
    @IBOutlet weak var confirmPwd: UITextField!
    @IBOutlet weak var imv_profile: UIImageView!
    
    var pictureData = [CapturePicModel]()
    var imagePicker: ImagePicker!
    var imageFils = [String]()
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.editInitAll()
        imv_profile.addTapGesture(tapNumber: 1, target: self, action: #selector(onEdtPhoto))
        //loadLayout()
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        /*let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditCategoriesVC") as! EditCategoriesVC
        vc.delegate = self*/
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
//    override func viewWillLayoutSubviews() {
//
//        if user!.type == PARAMS.TEACHER{
//            self.uiv_categories.isHidden = false
//            self.lbl_catName.text = editCategory
//        }
//    }
    override func viewDidAppear(_ animated: Bool) {
        if user!.type == PARAMS.TEACHER{
            self.uiv_categories.isHidden = false
            self.lbl_catName.text = editCategory
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func onEdtPhoto(gesture: UITapGestureRecognizer) -> Void {
        self.imagePicker.present(from: imv_profile)
    }
    
    func gotoUploadProfile() {
        for one in pictureData {
            let image = one.imgFile!
            imageFils.append(saveToFile(image:image,filePath:"photo",fileName:randomString(length: 2))) // Image save action
        }
        imv_profile.image = pictureData.last?.imgFile
    }
    
    func editInitAll() {
        let url = URL(string: user!.photo ?? "") // must be chaged
        imv_profile.kf.setImage(with: url,placeholder: UIImage(named: "account_photo"))
        
        if UserDefault.getBool(key: PARAMS.SOCIAL_LOGIN,defaultValue: false){
            edtName.text = user!.username
            edtEmail.text = user!.email
            edtEmail.textColor = .lightGray
            edtEmail.isUserInteractionEnabled = false
            edtPwd.text = ""
            edtPwd.textColor = .darkGray
            edtPwd.isUserInteractionEnabled = false
            edtPwd.isEnabled = false
            confirmPwd.text = ""
            confirmPwd.textColor = .darkGray
            confirmPwd.isUserInteractionEnabled = false
            confirmPwd.isEnabled = false
            edtphoneNumber.text = ""
            //edtphoneNumber.textColor = .darkGray
            //edtphoneNumber.isUserInteractionEnabled = false
            //edtphoneNumber.isEnabled = false
            setEdtPlaceholderColor(edtPwd, placeholderText: "Access Denied", placeColor: UIColor.lightGray)
            //setEdtPlaceholderColor(edtphoneNumber, placeholderText: "Access Denied", placeColor: UIColor.lightGray)
            setEdtPlaceholderColor(confirmPwd, placeholderText: "Access Denied", placeColor: UIColor.lightGray)
        }
        else{
            edtName.text = user!.username
            edtEmail.text = user!.email
            edtphoneNumber.text = user!.phone
            edtPwd.text = ""
            confirmPwd.text = ""
        }
    }
    @IBAction func categoryBtnClicked(_ sender: Any) {
        self.gotoNavPresent1("EditCategoriesVC")
    }
    
    @IBAction func updateBtnClicked(_ sender: Any) {
        if UserDefault.getBool(key: PARAMS.SOCIAL_LOGIN,defaultValue: false){
            if edtName.text == "" || edtphoneNumber.text == ""{
                let toast = NSLocalizedString("Please input all information correctly.\n Phone number must be 12 digits.", comment: "")
                self.alertDisplay(alertController: self.alertMake(toast))
            }
            else{
                editApi(email :edtEmail.text!, phone: edtphoneNumber.text!, username : edtName.text!, password : edtPwd.text!,categorie:lbl_catName.text! )
            }
        }
        else{
            validator.registerField(edtName, errorLabel: nil , rules: [RequiredRule()])
            validator.registerField(edtEmail, errorLabel: nil , rules: [RequiredRule(),EmailRule()])
            validator.registerField(edtPwd, errorLabel: nil , rules: [RequiredRule(),AlphaNumericRule()])
             validator.registerField(edtphoneNumber, errorLabel: nil , rules: [RequiredRule(),MinLengthRule(length: 11)])
            validator.registerField(confirmPwd, errorLabel: nil , rules: [RequiredRule(),ConfirmationRule(confirmField: edtPwd)])
            validator.styleTransformers(success:{ (validationRule) -> Void in
                
                // clear error label
                validationRule.errorLabel?.isHidden = true
                validationRule.errorLabel?.text = ""
             

                
            }, error:{ (validationError) -> Void in
                print("error")

                let toast = NSLocalizedString("Please input all information correctly.\n Phone number must be 12 digits.", comment: "")
                self.alertDisplay(alertController: self.alertMake(toast))
                
            })
            validator.validate(self)
        }
    }
    
    func validationSuccessful() {
        editApi(email :edtEmail.text!, phone: edtphoneNumber.text!, username : edtName.text!, password : edtPwd.text!, categorie: lbl_catName.text!)
    }
    
    func editApi( email : String, phone: String,username : String, password : String,categorie: String){
        let toast = NSLocalizedString("Connecting... \n Just a moment!", comment: "")
        self.showProgressSet_withMessage(toast, msgOn: false, styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
        
        ApiManager.editProfile(email: email, phone: phone, username: username, password: password,categorie:categorie, images: imageFils, user_id: user!.id){
            (isSuccess, data) in
            self.hideProgress()
            
            if isSuccess{
                let result_message = data as! String
                if(result_message == "0"){
                    UserDefault.setString(key: PARAMS.EMAIL, value: email)
                    self.updateSuccess(email)
                }
                else if(result_message == "1"){
                    self.alertDisplay(alertController: self.alertMake("File Upload Failed!"))
                }
                else {
                    let toast = NSLocalizedString("Please select user photo!", comment: "")
                    self.alertDisplay(alertController: self.alertMake(toast))
                }
            }
            else{
                if data == nil{
                    self.alertDisplay(alertController: self.alertMake("Connection Error!"))
                    print("connection Error")
                }
                else{
                    let result_message = data as! String
                    if(result_message == "1"){
                        let toast = NSLocalizedString("Email already Exist!", comment: "")
                        self.alertDisplay(alertController: self.alertMake(toast))
                    }
                    
                    else{
                        self.alertDisplay(alertController: self.alertMake("Network Error!"))
                    }
                }
            }
        }
    }
    
    func updateSuccess(_ email : String){
        ApiManager.getMyDetail(email: email){
            (isSuceess, data) in
            if isSuceess{
                let dict = JSON(data as Any)
                user?.clearUserInfo()
                user = UserModel(dict)
                user?.saveUserInfo()
                UserDefault.setBool(key: Constants.LOGOUT, value: false)
                if user!.isValid{
                    let toast = NSLocalizedString("User Profile Updated!", comment: "")
                    self.alertDisplay(alertController: self.alertMake(toast))
                }
                else{
                    return
                }
            }
        }
    }
   
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        print("validation error")
    }
}

extension editProfileVC : ImagePickerDelegate { // custom uiImagePicker
    
    func didSelect(image: UIImage?) {
        self.pictureData.removeAll()
        if let image = image {
        let one = CapturePicModel(image) // data add
        pictureData.append(one) // append action
        self.gotoUploadProfile()
            
        }
        else{
            print("No picture")
        }
    }
}
 


