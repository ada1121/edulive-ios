
//  ContentListVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/25/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import SwipeCellKit
import CryptoKit
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG
import Alamofire
import SwiftyXMLParser
import SwiftyJSON

class AchievementVC: BaseVC1 {

    @IBOutlet weak var ui_collectionView: UICollectionView!
    var medalDataSource = [MedalModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataInit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    func dataInit()  {
        let images = ["medal1.png","medal2.png","medal3.png","medal4.png","medal5.png","medal5.png"]
        let titles = ["Quick Learner","Master Mind!","Super Learner","The Achiiever","Locked Badge","Locked Badge"]
        let contents = ["Completed 1 course","Got 1st place on leaderboard","Completed more than 5 courses","Logged in everyday for a month","Unlock to see the details","Unlocked to see the details"]
        
        for i in 0 ... 5{
            medalDataSource.append(MedalModel(image : images[i],title : titles[i],content : contents[i]))
        }
    }
    // for the back button click event
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}

extension AchievementVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return medalDataSource.count
    }
    
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MedalCell", for: indexPath) as! MedalCell
        
        cell.entity = medalDataSource[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}

extension AchievementVC : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let h: CGFloat = 100
        let w = collectionView.frame.size.width
        
        return CGSize(width: w, height: h)
        
    }
}

class MedalModel{
    var image = ""
    var title = ""
    var content = ""
    init(image : String, title : String, content : String) {
        self.image = image
        self.title = title
        self.content = content
    }
}

class MedalCell: UICollectionViewCell {
    
    @IBOutlet var imv_medal: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var content: UILabel!
    
    var entity:MedalModel!{
        
        didSet{
            imv_medal.image = UIImage(named: entity.image)
            title.text = entity.title
            content.text = entity.content
        }
    }
}
