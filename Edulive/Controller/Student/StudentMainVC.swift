
//
//  MainVC.swift
//  Edulive
//
//  Created by Mac on 2/14/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import SwipeCellKit
import SwiftyJSON
import SwiftyUserDefaults

class StudentMainVC : BaseVC1 {
    
    @IBOutlet weak var ui_collectionView: UICollectionView!
    @IBOutlet weak var col_featured: UICollectionView!
    
    var photoHeight : Int = 0
    let slider = UISlider()
    let sliderlabel = UILabel()
    var amount = 100
    
    override func viewDidLoad() {
        super.viewDidLoad()
        networkcondition1 = 1
        //subjectcollectionInit()
        contentDataSource.removeAll()
        favoriteDataSource.removeAll()
        self.getFavoritesLessons()
        subIndex = 0
        self.initPaymentRefer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        global_amount = 100
        self.col_featured.reloadData()
        self.ui_collectionView.reloadData()
    }
    
    func getFavoritesLessons() {
        ApiManager.getcontentFavorites(user_id: user!.id){
            (isSuccess, data) in
            if isSuccess{
                let favoriteList = JSON(data as Any)
                var count = 0
                for one in favoriteList{
                    let jsoncontenlist = JSON(one.1)
                    favoriteDataSource.append(ContentModel(jsoncontenlist))
                    count += 1
                    if count == jsoncontenlist.count{
                        
                    }
                }
            }
        }
    }
    
    @IBAction func viewallBtnClicked(_ sender: Any) {
        self.gotoNavPresent1("ViewAllVC")
    }
    @IBAction func categoriesViewAll(_ sender: Any) {
        self.gotoNavPresent1("ViewAllCategoriesVC")
    }
}

extension StudentMainVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == ui_collectionView{
            return subDataSource.count
        }
        else{
            return allcontentDataSource.count
        }
    }
    
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == ui_collectionView{
            if subDataSource.count > indexPath.row{
               let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionCell
                cell.entity = subDataSource[indexPath.row]
                return cell
            }
            else{
               let cell = ui_collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionCell
                return cell
            }
        }
        else {
            if allcontentDataSource.count > indexPath.row{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AllContentsCell", for: indexPath) as! AllContentsCell
                cell.entity = allcontentDataSource[indexPath.row]
                return cell
            }
            else{
                let cell = col_featured.dequeueReusableCell(withReuseIdentifier: "AllContentsCell", for: indexPath) as! AllContentsCell
                return cell
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    contentDataSource.removeAll()
    
    print("clicked")
    if collectionView == ui_collectionView{
        
        showProgressSet_withMessage("Loading \(subDataSource[indexPath.row].subName) Lessons!\n Please wait...", msgOn: true, styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
        subjectCaption = subDataSource[indexPath.row].subName
        subIndex = subDataSource[indexPath.row].id.toInt()!
        let userid = user!.id
        
        DispatchQueue.main.async(execute:  {
            ApiManager.getContent(categorie_id: subIndex, amount: self.amount,user_id: userid!){
                (isSuccess, data) in
                if isSuccess{
                    self.hideProgress()
                    let contentList = JSON(data as Any)
                    //var count = 0
                    if contentList.count == 0{
                        self.alertDisplay(alertController: self.alertMake("There is no any lessons with your conditions!"))
                    }
                    else{
                        self.gotoNavPresent1("ContentListVC")
                    }
                }
            }
        })
    }
    else{
        //print("featured colleciton clicked")
        let pathrow = indexPath.row
        descriptionText = allcontentDataSource[pathrow].description
        str_payprice = allcontentDataSource[pathrow].amount
        str_payContentId = allcontentDataSource[pathrow].id
        str_paycontentName = allcontentDataSource[pathrow].title
        str_paidTeacherId = allcontentDataSource[pathrow].user_id
        str_paidTeacherName = allcontentDataSource[pathrow].username
        str_paidContentTitle = allcontentDataSource[pathrow].title
        self.gotoNavPresent1("Description")
    }
}
    
    func initPaymentRefer()  {
        str_payContentId = ""
        str_payprice = ""
        str_paycontentName = ""
        str_paidTeacherId = ""
        str_paidTeacherName = ""
    }
}

extension StudentMainVC: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == ui_collectionView{ // categories type
            let w = collectionView.frame.size.width / 3
            let h: CGFloat = w * 1.5
            return CGSize(width: w, height: h)
        }
        else{
            let w = collectionView.frame.size.width / 3
            let h: CGFloat = collectionView.frame.size.height
            return CGSize(width: w, height: h)
        }
    }
}


