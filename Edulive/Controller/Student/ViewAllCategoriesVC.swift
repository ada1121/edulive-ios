//
//  ViewAllCategoriesVC.swift
//  Edulive
//
//  Created by Mac on 3/14/20.
//  Copyright © 2020 Mac. All rights reserved.
//


import UIKit
import SwipeCellKit
import SwiftyJSON
import SwiftyUserDefaults
import Alamofire
import SwiftyXMLParser
import CryptoKit
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG

class ViewAllCategoriesVC : BaseVC1 {

    @IBOutlet weak var ui_collectionView: UICollectionView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
    }
    
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}

    extension ViewAllCategoriesVC : UICollectionViewDelegate, UICollectionViewDataSource{
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return subDataSource.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionCell
            cell.entity = subDataSource[indexPath.row]
            return cell
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            showProgressSet_withMessage("Loading \(subDataSource[indexPath.row].subName) Lessons!\n Please wait...", msgOn: true, styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
                   subjectCaption = subDataSource[indexPath.row].subName
                   subIndex = subDataSource[indexPath.row].id.toInt()!
                   let userid = user!.id
                   
                   DispatchQueue.main.async(execute:  {
                       ApiManager.getContent(categorie_id: subIndex, amount: 100,user_id: userid!){
                           (isSuccess, data) in
                           if isSuccess{
                               self.hideProgress()
                               let contentList = JSON(data as Any)
                               //var count = 0
                               if contentList.count == 0{
                                   self.alertDisplay(alertController: self.alertMake("There is no any lessons with your conditions!"))
                               }
                               else{
                                   self.gotoNavPresent1("ContentListVC")
                               }
                           }
                       }
                   })
        }
    }

    extension ViewAllCategoriesVC : UICollectionViewDelegateFlowLayout{
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
            let w = collectionView.frame.size.width / 2.1
            let h = w
            return CGSize(width: w, height: h)
        }
    }


