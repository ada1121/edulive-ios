//
//  Description.swift
//  Edulive
//
//  Created by Mac on 2/20/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import CryptoKit
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG
import Alamofire
import SwiftyXMLParser
import SwiftyJSON

class Description: BaseVC1 {
    
    @IBOutlet weak var content: UITextView!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var lbl_des: UILabel!
    
    var wechatmodel : WechatProductModel?
    var req_prepay_id = ""
    var req_nonce_str = ""
    var req_sign = ""
    var req_timeStamp : UInt32!
    @IBOutlet weak var lbl_btnCaption: UILabel!
    
    @IBOutlet weak var imv_des: UIImageView!
    
    @IBOutlet weak var btn_paynow: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let randomInt = (0...9).randomElement()
        imv_des.image = UIImage.init(named: "book" + "\(randomInt ?? 0)" + ".png")
        
        NotificationCenter.default.addObserver(self, selector: #selector(onWechatPaySuccess(_:)), name: Notification.Name.wechat_pay_success, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onWechatPayFail(_:)), name: Notification.Name.wechat_pay_fail, object: nil)
        
        //WeChatResponseHandller.shared.delegate = self
        content.text = descriptionText
        lbl_des.text = str_paidContentTitle
        
        if str_payprice == "0" || str_payprice == ""{
            let free = NSLocalizedString("free", comment: "")
            let join_now = NSLocalizedString("Join Now", comment: "")
            
            lbl_price.text = free
            lbl_btnCaption.text = join_now
        }
        else{
           let pay_now = NSLocalizedString("Pay Now", comment: "")
           lbl_price.text = "$" + "" + str_payprice
           lbl_btnCaption.text = pay_now
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if user!.type == PARAMS.TEACHER{
            self.btn_paynow.isHidden = true
            self.lbl_btnCaption.isHidden = true
        }
    }
    @objc func onWechatPaySuccess(_ notification : Notification) {
        
        print(notification)
        self.progShowSuccess(true, msg: "Pay Success!")
        confRoomName = ""
        confRoomName = str_payContentId
        str_payContentId = ""
        str_payprice = ""
        str_paycontentName = ""
        str_paidTeacherId = ""
        str_paidTeacherName = ""
        self.gotoVC("BroadCastNav")
    }
    
    @objc func onWechatPayFail(_ notifcaion : Notification) {
        showToast(NSLocalizedString("error", comment: ""))
    }
    
    @IBAction func payNow(_ sender: Any) {
        if str_payprice != "0" && str_payprice != ""{
            self.showProgress()
            ApiManager.getPaid(user_id: user!.id, content_id: str_payContentId){
                (isSuccess, data) in
                self.hideProgress()
                if isSuccess{
                    confRoomName = str_payContentId
                    self.gotoVC("BroadCastNav")
                }
                else{
                    let stringParams = self.toXml(self.makeWechatModel().toDictionary())
                    print(stringParams)
                    
                    let url = URL(string:FIRST_REQUEST)
                    var xmlRequest = URLRequest(url: url!)
                    xmlRequest.httpBody = stringParams.data(using: String.Encoding.utf8, allowLossyConversion: true)
                    xmlRequest.httpMethod = "POST"
                    xmlRequest.addValue("application/xml", forHTTPHeaderField: "Content-Type")

                    
                    //let request = Alamofire.request(xmlRequest)
                    //debugPrint("**********",request)
                    Alamofire.request(xmlRequest)
                        .responseData { (response) in
                            let stringResponse: String = (String(data: response.data!, encoding: String.Encoding.utf8) as String?)!
                            debugPrint(stringResponse)
                            self.gotoXmlparser(stringResponse)
                    }
                }
            }
        }
        else{
            confRoomName = str_payContentId
            self.gotoVC("BroadCastNav")
        }
    }
    
    func sendPayReq() {
        
        let request = PayReq()
        request.partnerId = Constants.WECHAT_MERCHANT_NUMBER
        request.prepayId = req_prepay_id
        request.nonceStr = req_nonce_str
        request.timeStamp = req_timeStamp
        request.package = "Sign=WXPay"
        request.sign = req_sign
        wechatRequest = 3
        WXApi.send(request)
    }
    
    @IBAction func gotoBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    func gotoXmlparser(_ xmlString : String) {
        
        let xml = try! XML.parse(xmlString)
        if let perpay_id = xml["xml", "prepay_id"].text {
            req_prepay_id = perpay_id
        }
        if let nonce_str = xml["xml", "nonce_str"].text {
            req_nonce_str = nonce_str
        }
        self.makeReq_sign()
    }
    
    func makeReq_sign() {
        
        var returnedString = ""
        req_timeStamp = UInt32(NSDate().timeIntervalSince1970)
        returnedString = "appid=" + Constants.WECHAT_ID  + "&" + "noncestr=" + req_nonce_str + "&" + "package=" + "Sign=WXPay" + "&" + "partnerid=" + Constants.WECHAT_MERCHANT_NUMBER  + "&" + "prepayid=" + req_prepay_id + "&" + "timestamp=" + "\(req_timeStamp ?? 0)" + "&" + "key=" + Constants.WECHAT_API_KEY
        req_sign = "\(MD5(string: returnedString).map { String(format: "%02hhx", $0) }.joined())".uppercased()
        self.sendPayReq()
    }
    
    func toXml(_ dict: Dictionary<AnyHashable, Any>) -> String {
        
        var str = "<xml>"
        for (key, value) in dict {
            str.append("<" + "\(key)" + ">")
            str.append("\(value)")
            str.append("</" + "\(key)" + ">")
        }
        str.append("</xml>")
        print(str)
        return str
    }
        
    func makeWechatModel() -> WechatProductModel {
        
        let d = randomString(length: 5)
        let nonce_str = "\(MD5(string: d).map { String(format: "%02hhx", $0) }.joined())"
        let out_trade_no = Int(NSDate().timeIntervalSince1970) * 1000
        let user_id = user?.id
        let username = user?.username
        let attached = "\(user_id ?? 0)" + "_" + username! + "_" + str_payContentId + "_" + str_paycontentName + "_" + str_paidTeacherId + "_" + str_paidTeacherName + "_" + str_payprice// user_id,user_name,content_id, content_name, teacher_id, teacher_name, amount
        let body = "12months"
        let paidamout = str_payprice.toInt()!
        wechatmodel = WechatProductModel(appid: Constants.WECHAT_ID, mch_id: Constants.WECHAT_MERCHANT_NUMBER, nonce_str: "\(nonce_str)", sign:Constants.WECHAT_API_KEY, body: body, out_trade_no: "\(out_trade_no)", total_fee: paidamout * 100, spbill_create_ip: "127.0.0.1", notify_url: Constants.WECHAT_NOTIFY_URL, trade_type: Constants.WECHAT_TRADE_TYPE, attach: attached)
        let sign_original = (wechatmodel?.toString())! + "key=" + Constants.WECHAT_API_KEY
        let sign = "\(MD5(string: sign_original).map { String(format: "%02hhx", $0) }.joined())".uppercased()
        wechatmodel?.sign = sign
        return wechatmodel!
    }
        
    func MD5(string: String) -> Data {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: length)

        _ = digestData.withUnsafeMutableBytes { digestBytes -> UInt8 in
            messageData.withUnsafeBytes { messageBytes -> UInt8 in
                if let messageBytesBaseAddress = messageBytes.baseAddress, let digestBytesBlindMemory = digestBytes.bindMemory(to: UInt8.self).baseAddress {
                    let messageLength = CC_LONG(messageData.count)
                    CC_MD5(messageBytesBaseAddress, messageLength, digestBytesBlindMemory)
                }
                return 0
            }
        }
        return digestData
    }
}

