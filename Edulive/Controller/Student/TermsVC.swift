//
//  TermsVC.swift
//  Edulive
//
//  Created by Mac on 3/12/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import WebKit

class TermsVC: BaseVC1 {
    
    @IBOutlet weak var webView: WKWebView!
    @IBAction func gotoHome(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            // 1
            let url = URL(string: "http://www.eduintel.cn/home/privacy_policy")!
            webView.load(URLRequest(url: url))
            
            // 2
    //        let refresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: webView, action: #selector(webView.reload))
    //        toolbarItems = [refresh]
    //        navigationController?.isToolbarHidden = false
        }
}
