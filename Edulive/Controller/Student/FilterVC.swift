//
//  MainVC.swift
//  Edulive
//
//  Created by Mac on 2/14/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import SwipeCellKit
import SwiftyJSON
import SwiftyUserDefaults

class FilterVC : BaseVC1 {
    
    @IBOutlet weak var ui_collectionView: UICollectionView!
    @IBOutlet weak var teacherColloectionView: UICollectionView!
    @IBOutlet weak var txtSearchBar: UITextField!
    @IBOutlet var tblView: UITableView!
    
    //Variables for search
    var searchActive : Bool = false
    var data :[String] = []
    var filtered:[String] = []
    
    var photoHeight : Int = 0
    let slider = UISlider()
    let sliderlabel = UILabel()
    var allDataSource = [ContentModel]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        global_amount = Int(slider.value)
        self.getAllData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //subjectcollectionInit()
        contentDataSource.removeAll()
        student_teacherlessonsDataSource.removeAll()
        self.initSlider()
        txtSearchBar.addPadding(.left(16))
        txtSearchBar.delegate = self
        txtSearchBar.addTarget(self, action: #selector(FilterVC.textFieldDidChange(_:)),
        for: UIControl.Event.editingChanged)
        tblView.isHidden = true
    }
    
    func getAllData() {
        
        allDataSource.removeAll()
        ApiManager.getAllContents(){
            (isSuccess, data) in
            if isSuccess{
                let jsondata = JSON(data as Any)
                var num = 0
                for one in jsondata{
                    num += 1
                    let jsonTotalContents = JSON(one.1)
                    self.allDataSource.append(ContentModel(jsonTotalContents))
                    if num == jsondata.count{
                        var loop = 0
                        self.data.removeAll()
                        for one in self.allDataSource{
                            loop += 1
                            self.data.append(one.title)
                            if loop == self.allDataSource.count{
                                self.tblView.reloadData()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func initSlider(){
        
        slider.frame = CGRect(x: 30, y: 0, width: 250, height: 35)
        sliderlabel.frame = CGRect(x: 300, y: 0, width: 250, height: 35)
        
        slider.centerY = UIScreen.main.bounds.height * 0.43 + UIScreen.main.bounds.height * 0.2 * 0.5
        sliderlabel.centerY = slider.centerY

        slider.minimumTrackTintColor = .white
        slider.maximumTrackTintColor = .lightGray
        slider.thumbTintColor = .white

        slider.maximumValue = 100
        slider.minimumValue = 0
        
        slider.setValue(100, animated: false)

        slider.addTarget(self, action: #selector(FilterVC.changeVlaue(_:)), for: .valueChanged)
        sliderlabel.textColor = .white
        sliderlabel.backgroundColor = .clear
        self.view.addSubview(slider)
        self.view.addSubview(sliderlabel)
    }
   
    @objc func changeVlaue(_ sender: UISlider) {
        //print("value is" , Int(sender.value))
        global_amount = Int(sender.value)
        sliderlabel.text = "$ \(Int(sender.value))"
    }
    @IBAction func applyBtnClicked(_ sender: Any) {
        self.gotoNavPresent1("FilterShowVC")
    }
}

extension FilterVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == ui_collectionView{
            return subDataSource.count
        }
        else{
            return recommendedTeacherDatasource.count
        }
    }
    
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        if collectionView == ui_collectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionCell
                cell.entity = subDataSource[indexPath.row]
                return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TeacherCollectionCell", for: indexPath) as! TeacherCollectionCell
                cell.entity = recommendedTeacherDatasource[indexPath.row]
                return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        contentDataSource.removeAll()
        
        print("clicked")
        if collectionView == ui_collectionView{
            showProgressSet_withMessage("Loading \(subDataSource[indexPath.row].subName) Lessons!\n Please wait...", msgOn: true, styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
            subjectCaption = subDataSource[indexPath.row].subName
            subIndex = subDataSource[indexPath.row].id.toInt()!
            let userid = user!.id
            
            DispatchQueue.main.async(execute:  {
                ApiManager.getContent(categorie_id: subIndex, amount: 100,user_id: userid!){
                   (isSuccess, data) in
                   if isSuccess{
                       self.hideProgress()
                       let contentList = JSON(data as Any)
                       //var count = 0
                        if contentList.count == 0{
                            self.alertDisplay(alertController: self.alertMake("There is no any lessons with your conditions!"))
                        }
                        else{
                            self.gotoNavPresent1("ContentListVC")
                        }
//                       for one in contentList{
//                           let jsoncontenlist = JSON(one.1)
//                           contentDataSource.append(ContentModel(jsoncontenlist))
//                           count += 1
//
//                           if count == contentList.count{
//                               self.gotoNavPresent1("ContentListVC")
//                           }
//                       }
                   }
               }
            })
        }
        else{
            let pathrow = indexPath.row
            let teacher_id = recommendedTeacherDatasource[pathrow].id
            self.getteacherLessonsFromRecommended(teacher_id)
        }
    }
    
    func getteacherLessonsFromRecommended(_ teacher_id : Int){
        let toast = NSLocalizedString("Connecting... \n Just a moment!", comment: "")
        self.showProgressSet_withMessage(toast, msgOn: false, styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
        ApiManager.getMyAllContents(user_id: teacher_id){
            (isSuccess, data) in
            self.hideProgress()
            if isSuccess{
                self.hideProgress()
                let contentList = JSON(data as Any)
                var count = 0
                 student_teacherlessonsDataSource.removeAll()
                
                if contentList.count == 0{
                    self.alertDisplay(alertController: self.alertMake("There is no any lessons for this teahcer!"))
                }
                else{
                    for one in contentList{
                        let jsoncontenlist = JSON(one.1)
                        student_teacherlessonsDataSource.append(InstructorModel(jsoncontenlist))
                        count += 1
                        if count == contentList.count{
                            self.gotoNavPresent1("InstructorVC")
                        }
                    }
                }
            }
        }
    }
}

extension FilterVC : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == ui_collectionView{
            let w = collectionView.frame.size.width / 3
            let h: CGFloat = w * 1.5
            return CGSize(width: w, height: h)
        }
        else{
            let w = collectionView.frame.size.width / 3
            let h: CGFloat = collectionView.frame.size.height
            return CGSize(width: w, height: h)
        }
    }
}

extension FilterVC: UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        // filter tableViewData with textField.text
        
        let searchText  = textField.text
        
        filtered = data.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            let range = tmp.range(of: searchText!, options: NSString.CompareOptions.caseInsensitive)
            /*print("range =======",range)
            print("rangeLocation =======",range.location)
            print("Nsfound =======",range.location != NSNotFound)*/
            return range.location != NSNotFound
        })
        print(filtered)
        if(filtered.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        if searchActive{
            tblView.isHidden = false
        }
        else{
            tblView.isHidden = true
        }
        self.tblView.reloadData()
        
    }
    
   /* func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        isFiltered = true
        searchResults = prods.filter({(coisas:String) -> Bool in
            let stringMatch = coisas.range(of: textField.text!)     //rangeOfString(textField.text)
            return stringMatch != nil
            
        })
        print(searchResults.description)
        textField.resignFirstResponder()
        tblView.reloadData()
        return true
    } */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(searchActive){
            return filtered.count
        } else {
            return data.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchTableViewCell", for: indexPath) as! searchTableViewCell
        
        if(searchActive){
            cell.textlabelSearch.text = filtered[indexPath.row]
        } else {
            if allDataSource.count > indexPath.row{
                cell.textlabelSearch.text = allDataSource[indexPath.row].title
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.getDataSourceidFromFilterd().count > indexPath.row {
            
            print(self.getDataFromID(self.getDataSourceidFromFilterd()[indexPath.row]))
            /*if user!.type == "student"{*/
             descriptionText = self.getDataFromID(self.getDataSourceidFromFilterd()[indexPath.row]).description
             str_payprice = self.getDataFromID(self.getDataSourceidFromFilterd()[indexPath.row]).amount
             str_payContentId = self.getDataFromID(self.getDataSourceidFromFilterd()[indexPath.row]).id
                print("id ==========",str_payContentId)
             str_paycontentName = self.getDataFromID(self.getDataSourceidFromFilterd()[indexPath.row]).title
             str_paidTeacherId = self.getDataFromID(self.getDataSourceidFromFilterd()[indexPath.row]).user_id
             str_paidTeacherName = self.getDataFromID(self.getDataSourceidFromFilterd()[indexPath.row]).username
            str_paidContentTitle = self.getDataFromID(self.getDataSourceidFromFilterd()[indexPath.row]).title

                if str_payContentId != "" {
                    self.gotoNavPresent1("Description")
                }
                else{
                    return
                }
            /*}
            else{
                return
            }*/
        }
    }
    
    func getDataSourceidFromFilterd() -> [String] {
        var dataSourceid = [String]()
        for one in allDataSource{
            for two in filtered{
                if one.title == two{
                    dataSourceid.append(one.id)
                }
            }
        }
        print(dataSourceid.removeDuplicates())
        return dataSourceid.removeDuplicates()
    }
    func getDataFromID(_ id: String) -> ContentModel {
        var returnModel : ContentModel?
        for one in allDataSource{
            if id == one.id{
                returnModel = one
            }
        }
        return returnModel ?? ContentModel()
    }
}

