//
//  IntroVC.swift
//  Edulive
//
//  Created by Mac on 2/13/20.
//  Copyright © 2020 Mac. All rights reserved.
//


import UIKit
import CHIPageControl
import SwiftyJSON

class IntroVC : BaseVC1 {

    internal let numberOfPages = 3
    var progress = 0.0
    
    @IBOutlet var pageControls: [CHIBasePageControl]!
    @IBOutlet weak var ui_collection: UICollectionView!
    @IBOutlet weak var ui_skipBut: UIButton!
      
      var datasource = [IntroModel]()
      
      let imgNames = ["welcome1", "welcome2", "welcome3"]
      let Conttitle = ["Welcome to Our Program",
                  "Lorem Issue Addash",
                  "Lorem Issue Addash"
                  ]
      let Lbltop = [
          "Lorem Issue Addash Lorem Radis Domiss",
          "Lorem Issue Addash Lorem Radis",
          "Lorem Issue Addash Lorem Radi Dsahcie"
          
      ]
    
    let Lblbottom = [
           "Lorem Issue Addash  Radis",
           "Lorem Issue Addash Lorem Radis",
           "Lorem Issue Addash Lorem "
       ]
    
    let lblBtn = [
        NSLocalizedString("Next", comment: ""),
        NSLocalizedString("Next", comment: ""),
        NSLocalizedString("Register", comment: "")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        self.pageControls.forEach { (control) in
            control.numberOfPages = self.numberOfPages
        }
        
        for i in 0 ..< imgNames.count {
            
            let one = IntroModel(imgBack: imgNames[i], title: Conttitle[i], lbltop: Lbltop[i], lblbottom: Lblbottom[i], lblbtn: lblBtn[i])
            datasource.append(one)
        }
    }
   
    @IBAction func onClickNext(_ sender: Any) {
          
          progress += 1.0
          if progress == 3.0 {
            self.gotoVC("LoginVC")
            return
          }
          ui_collection.scrollToItem(at: IndexPath.init(row: Int(progress), section: 0) , at: .centeredHorizontally, animated: true)
          self.pageControls.forEach { (control) in
              control.progress = progress
          }
      }
    
    @IBAction func onClickSkip(_ sender: Any) {
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        loginVC = (storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC)
//        loginNav = NavigationController(rootViewController: loginVC)
//
//        loginNav!.modalPresentationStyle = .fullScreen
//        self.present(loginNav!, animated: false, completion: nil)
        self.gotoVC("LoginVC")
    }

    func showSkipButton() {
        if progress < 2.0 {
            ui_skipBut.isHidden = false
        } else {
            ui_skipBut.isHidden = true
        }
    }
    
}

extension IntroVC : UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroCell", for: indexPath) as! IntroCell
        cell.entity = datasource[indexPath.row]
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let total = scrollView.contentSize.width - scrollView.bounds.width
        let offset = scrollView.contentOffset.x
        let percent = Double(offset / total)
        progress = percent * Double(self.numberOfPages - 1)
        
        self.pageControls.forEach { (control) in
            control.progress = progress
        }
        
        showSkipButton()
    }
}

extension IntroVC : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let w = collectionView.frame.size.width
        let h = collectionView.frame.size.height
        
        return CGSize(width: w, height: h)
    }
}
