//
//  LoginVC.swift
//  FoodTracking
//
//  Created by Ubuntu on 12/30/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import Foundation
import UIKit
import SwiftValidator
import IQKeyboardManagerSwift
import SwiftyJSON
import SwiftyUserDefaults
import GDCheckbox
import KRProgressHUD
import KRActivityIndicatorView
import Alamofire

var networkconnection2 :Int = 0

class LoginVC: BaseVC1 , ValidationDelegate, UITextFieldDelegate {

    @IBOutlet weak var loginCaption: UILabel!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var edtEmail: UITextField!
    @IBOutlet weak var edtPwd: UITextField!
    
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        networkcondition1 = 1
        editInit()
        navBarHidden()
        let login_here = NSLocalizedString("LOGIN HERE", comment: "")
        loginCaption.text = login_here
        loadLayout()
        NotificationCenter.default.addObserver(self, selector: #selector(onWechatSuccess(_:)), name: Notification.Name.wechat_success, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onWechatFail(_:)), name: Notification.Name.wechat_fail, object: nil)        
    }
    
    func editInit()  {
        
        let place_email = NSLocalizedString("Email", comment: "")
        let place_password = NSLocalizedString("Password", comment: "")
        setEdtPlaceholderColor(edtEmail, placeholderText: place_email, placeColor: UIColor.white)
        setEdtPlaceholderColor(edtPwd, placeholderText: place_password, placeColor: UIColor.white)
    }
    
    func loadLayout() {
//        edtEmail.text = "tony@gmail.com"
        edtEmail.text = "tansha@gmail.com"
        edtPwd.text = "123456"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        createGradientView(loginView)
    }
    
    @IBAction func forgotBtnClicked(_ sender: Any) {
        gotoVC("ForgotVC")
    }
    
    @IBAction func signBtnClicked(_ sender: Any) {
        gotoVC("SignUpNav")
    }
    
    @IBAction func loginWithWeChat(_ sender: Any) {
        wechatRequest = 1
       //TODO : we must implement wechat login
        let req = SendAuthReq()
        req.scope = "snsapi_userinfo" //Important that this is the same
        req.state = "com.medi.edulive.wechat_login" //This can be any random value
        if !WXApi.send(req) {
            showToast(NSLocalizedString("Please Installwechat", comment: ""))
        }
    }
    
    @objc func onWechatSuccess(_ notification : Notification) {
        
        print("reached login wechat success")
        if let data = notification.userInfo as? [String: String] {
            if let authcode = data["response"] {
                getWechatToken(authcode: authcode)
            }
        }
    }
    
    @objc func onWechatFail(_ notifcaion : Notification) {
        showToast(NSLocalizedString("error", comment: ""))
    }
    
    func getWechatToken(authcode : String) {
        
        let URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=\(Constants.WECHAT_ID)&secret=\(Constants.WECHAT_SECRET)&code=\(authcode)&grant_type=authorization_code"
        
        
        Alamofire.request(URL, method:.get)
            .responseJSON { response in
                
                switch response.result {
                    
                case .failure:
                    self.showToast(NSLocalizedString("connection_faild", comment: ""))
                    
                    
                case .success(let data):
                    
                    let dict = JSON(data)
                    print(dict)
                    let token = dict["access_token"].stringValue
                    if let openId = dict["openid"].string, !openId.isEmpty {
                    
                        self.getWhchatInfo(token: token, openId: openId)
                        //let accountId = "wx_\(openId)";
                        //self.socialLogin(accountId: accountId, type: 1)
                    }
                }
                
        }
    }
    
//    func socialLogin(accountId : String, type : Int) {
//
//        ApiManager.sharedManager.socialLogin(accountId: accountId) { (success, data) in
//
//            if (success) {
//                self.registerToken()
//            } else {
//
//                self.hideHUD()
//
//                if data == nil {
//                    self.showToast(NSLocalizedString("connection_faild", comment: ""))
//                } else {
//                    self.gotoPin(accountId : accountId, type : type)
//                }
//            }
//        }
//    }
//
    func getWhchatInfo(token : String, openId : String) {
        
        let URL = "https://api.weixin.qq.com/sns/userinfo?access_token=\(token)&openid=\(openId)"
        
        
        Alamofire.request(URL, method:.get)
            .responseJSON { response in
                
                switch response.result {
                    
                case .failure:
                    self.showToast(NSLocalizedString("connection_faild", comment: ""))
                    self.hideProgress()
                    
                case .success(let data):
                    
                    _ = "wx_"
                    let dict = JSON(data)
                    print(dict)
                    
                    let city = dict["city"].stringValue
                    let unionid = dict["unionid"].stringValue
                    _ = dict["headimgurl"].stringValue
                    let openid = dict["openid"].stringValue

                    if let nickname = dict["nickname"].string {
                        _ = nickname
                    } else {
                        _ = city
                    }
                    self.socialLogin(unionid, open_id: openid)
                }
        }
    }
    
    func socialLogin(_ union_id : String, open_id : String) {
        UserDefault.setBool(key: PARAMS.SOCIAL_LOGIN, value: true)
        self.loginApi(useremail: union_id, pwd: open_id)
    }
    
    @IBAction func loginBtnClicked(_ sender: Any) {
        
        validator.registerField(edtEmail, errorLabel: nil , rules: [ RequiredRule(),EmailRule()])
        validator.registerField(edtPwd, errorLabel: nil , rules: [RequiredRule(),AlphaNumericRule()])
        validator.styleTransformers(success:{ (validationRule) -> Void in
            
        }, error:{ (validationError) -> Void in
            print("error")
            self.progressSet( styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgcolor: .red, headerColor: .red, trailColor: .yellow)
            let toast = NSLocalizedString("Incorrect Email or Password", comment: "")
            self.progShowError(true, msg: toast)
        })
        validator.validate(self)
    }
    
    func validationSuccessful() {
        loginApi(useremail: edtEmail.text!, pwd: edtPwd.text!)
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        //showMessage1("Please Input Correct!")
    }
    
    func loginApi(useremail : String, pwd : String){
        
        let toast = NSLocalizedString("Connecting... \n Just a moment!", comment: "")
        self.showProgressSet_withMessage(toast, msgOn: true, styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
        
        ApiManager.login(useremail: useremail, password: pwd) { (isSuccess, data) in
            self.hideProgress()
            
            if isSuccess{
                let dict = JSON(data as Any)
                print(dict)
                user = UserModel(dict)
                user?.saveUserInfo()
                UserDefault.setBool(key: Constants.LOGOUT, value: false)
                if user!.isValid{
                    self.loginSuccess()

                }
            }
            else{
                UserDefault.setBool(key: PARAMS.SOCIAL_LOGIN, value: false)
                if data == nil{
                    let toast = NSLocalizedString("Connection Error!", comment: "")
                    self.alertDisplay(alertController: self.alertMake(toast))
                }
                else{
                    let result_message = data as! String
                    if(result_message == "1"){
                        let toast = NSLocalizedString("Non Exist Email!\n Please Sign Up!", comment: "")
                        self.alertDisplay(alertController: self.alertMake(toast))
                        
                    }
                    else{
                        let toast = NSLocalizedString("Password Incorrect", comment: "")
                        self.alertDisplay(alertController: self.alertMake(toast))

                    }
                }
            }
        }
    }
    
    func loginSuccess()  {
        
        AppDelegate.setupReferenceUITabBarController()
        AppDelegate.setupHHTabBarView()
        
        mainNav = NavigationController(rootViewController: AppDelegate.referenceUITabBarController)
        
        mainNav.modalPresentationStyle = .fullScreen
        self.present(mainNav, animated: true, completion: nil)
        
        //mainNav.setNavigationBarHidden(true, animated: true)
        mainNav.isNavigationBarHidden = true
        mainNav.navigationBar.setBackgroundImage(UIImage(), for: .default)
        mainNav.navigationBar.shadowImage = UIImage()
        mainNav.navigationBar.isTranslucent = true
        mainNav.view.backgroundColor = .clear
    }
}

