//
//  WelcomeVC.swift
//  Edulive
//
//  Created by Mac on 2/13/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class WelcomeVC: BaseVC1 {

    @IBOutlet weak var lbl_started: UILabel!
    @IBOutlet weak var welcomeContent: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        welcomeContent.text = "Welcome our EduLive! You can give Edulive, \n Anywhere Anyplaces You Want!"
    }
    @IBAction func clickStarted(_ sender: Any) {
        self.gotoVC("IntroVC")
    }
    @IBAction func clickLogin(_ sender: Any) {
        self.gotoVC("LoginVC")
    }
}



