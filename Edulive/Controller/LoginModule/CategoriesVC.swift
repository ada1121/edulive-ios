
//
//  DoctypeVC.swift
//  AlertaSegura
//
//  Created by Ubuntu on 1/30/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class CategoriesVC: BaseVC1{
    
    @IBOutlet weak var sampleTable: UITableView!
    var sampleArray = [String]()
    var cellHeight = 50
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getSampleArray()
    }

//    override func viewDidAppear(_ animated: Bool) {
//        sampleTable.roundCorners([.bottomLeft, .bottomRight], radius: 20)
//    }
//    
    func getSampleArray() {
        sampleArray.removeAll()
        for one in subDataSource{
            sampleArray.append(one.subName)
        }
    }
}

extension CategoriesVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sampleArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SampleCell.self)) as! SampleCell
        cell.sampleMeal.text = sampleArray[indexPath.row]
        cellHeight = Int(cell.bounds.size.height)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if categoriType == ""{
            categoriType = sampleArray[indexPath.row]
        }
        else{
            categoriType += ","
            categoriType += sampleArray[indexPath.row]
        }
        
        self.navigationController?.popToRootViewController(animated: false)
    }
}
class SampleCell: UITableViewCell {
    @IBOutlet weak var sampleMeal: UILabel!
}



