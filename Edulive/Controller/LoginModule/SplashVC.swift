//
//  SplashVC.swift
//  Edulive
//
//  Created by Mac on 2/12/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import SwiftyJSON
var networkcondition1 : Int = 0
class SplashVC: BaseVC1 {

    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + 30.0, execute: {
            if networkcondition1 == 0{
            self.progShowInfo(true, msg: "There is no connection with Server")
                self.gotoWelcomeVC()
            }
            if networkcondition1 == 1 {
                print("networkOK")
            }
        })
        
        self.teacherCollectionInit()
    }
    
    func teacherCollectionInit() {
        recommendedTeacherDatasource.removeAll()
        ApiManager.getRecommend(){
           (isSuccesss, data) in
           if isSuccesss{
                let recommendedTeachers = JSON(data as Any)
                for one in recommendedTeachers {
                    let json = JSON(one.1)
                    recommendedTeacherDatasource.append(TeacherModel(jsondata: json))
                }
                self.getCategories()
            }
        }
    }
    
    func getCategories() {
        subDataSource.removeAll()
        ApiManager.getCategories(){
            (isSuccess, data) in
            if isSuccess{
                let jsondata = JSON(data as Any)
                for one in jsondata{
                    let jsoncategories = JSON(one.1)
                    subDataSource.append(CollectionModel(jsoncategories))
                }
                self.getTotalUsers()
            }
        }
    }
    
    func getTotalUsers() {
        totalUserDataSource.removeAll()
        ApiManager.getUsers(){
            (isSuccess, data) in
            if isSuccess{
                let jsondata = JSON(data as Any)
                for one in jsondata{
                    let jsonTotalUsers = JSON(one.1)
                    totalUserDataSource.append(TotalUserModel(jsonTotalUsers))
                }
                self.getAllLessons()
            }
        }
    }
    
    func getAllLessons() {
        
        allcontentDataSource.removeAll()
        ApiManager.getAllContents(){
            (isSuccess, data) in
            if isSuccess{
                let jsondata = JSON(data as Any)
                var num = 0
                for one in jsondata{
                    num += 1
                    let jsonTotalContents = JSON(one.1)
                    allcontentDataSource.append(ContentModel(jsonTotalContents))
                    if num == jsondata.count{
                        self.getMyDetail()
                    }
                }
            }
        }
    }
    
    func getMyDetail() {
        if UserDefault.getBool(key: Constants.LOGOUT,defaultValue: false){
            self.gotoVC("LoginVC")
        }
        else{
            ApiManager.getMyDetail(email: user!.email ?? ""){
                (isSuceess, data) in
                if isSuceess{
                    user!.clearUserInfo()
                    let dict = JSON(data as Any)
                    user = UserModel(dict)
                    user?.saveUserInfo()
                    if UserDefault.getBool(key: Constants.LOGOUT){
                        if user!.isValid{
                            UserDefault.setBool(key: Constants.LOGOUT, value: false)
                            self.gotoVC("LoginVC")
                        }
                    }
                    else{
                        UserDefault.setBool(key: Constants.LOGOUT, value: false)
                        if user!.isValid{
                            self.gotoMain()
                        }
                    }
                }
                else{
                    self.gotoWelcomeVC()
                }
            }
        }
    }
    
    func gotoMain()  {
        
        AppDelegate.setupReferenceUITabBarController()
        AppDelegate.setupHHTabBarView()
        mainNav = NavigationController(rootViewController: AppDelegate.referenceUITabBarController)
        mainNav.modalPresentationStyle = .fullScreen
        self.present(mainNav, animated: true, completion: nil)
        mainNav.isNavigationBarHidden = true
        mainNav.navigationBar.setBackgroundImage(UIImage(), for: .default)
        mainNav.navigationBar.shadowImage = UIImage()
        mainNav.navigationBar.isTranslucent = true
        mainNav.view.backgroundColor = .clear
    }
    
    func gotoWelcomeVC()  {
        //DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
             let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
              let welcomeVC = (storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC)
            welcomeVC.modalPresentationStyle = .fullScreen
            self.present(welcomeVC, animated: true, completion: nil)
            networkcondition1 = 1
            //}
        //)
    }
}
