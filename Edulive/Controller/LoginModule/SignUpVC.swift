//
//  SignUpVC.swift
//  FoodTracking
//
//  Created by Ubuntu on 12/30/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import Foundation
import UIKit
import SwiftValidator
import IQKeyboardManagerSwift
import SwiftyJSON
import SwiftyUserDefaults
import GDCheckbox
import KRProgressHUD
import KRActivityIndicatorView
import Alamofire

class SignUpVC: BaseVC1, ValidationDelegate, UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var signBtnView: UIView!
    @IBOutlet weak var signCaption: UILabel!
    @IBOutlet weak var edtName: UITextField!
    @IBOutlet weak var edtEmail: UITextField!
    @IBOutlet weak var edtphoneNumber: UITextField!
    @IBOutlet weak var edtPwd: UITextField!
    @IBOutlet weak var confirmPwd: UITextField!
    @IBOutlet weak var checkBox: GDCheckbox!
    @IBOutlet weak var segMent: UISegmentedControl!
    @IBOutlet weak var categoriesView: UIView!
    @IBOutlet weak var editgroupBottomCons: NSLayoutConstraint!
    @IBOutlet weak var edtiTopcons: NSLayoutConstraint!
    @IBOutlet weak var categoriesLabel: UILabel!
    
    @IBOutlet weak var imv_profile: UIImageView!
    var pictureData = [CapturePicModel]()
    var imagePicker: ImagePicker!
    var imageFils = [String]()
    var segState : String?
    var categories : String?
    
    let validator = Validator()
    var socialLogin = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppDelegate().test()
        let signup_here = NSLocalizedString("SIGN UP HERE", comment: "")
        signCaption.text = signup_here
        editInit()
        imv_profile.addTapGesture(tapNumber: 1, target: self, action: #selector(onEdtPhoto))
        loadLayout()
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        NotificationCenter.default.addObserver(self, selector: #selector(onWechatSuccess1(_:)), name: Notification.Name.wechat_success1, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onWechatFail1(_:)), name: Notification.Name.wechat_fail1, object: nil)
    }
    
    override func viewWillLayoutSubviews() {
        self.categoriesLabel.text = categoriType
    }
    override func viewDidAppear(_ animated: Bool) {
        self.categoriesLabel.text = categoriType
    }
    
    @objc func onEdtPhoto(gesture: UITapGestureRecognizer) -> Void {
        self.imagePicker.present(from: imv_profile)
    }
    
    func gotoUploadProfile() {
        for one in pictureData {
            let image = one.imgFile!
            imageFils.append(saveToFile(image:image,filePath:"photo",fileName:randomString(length: 2))) // Image save action
        }
        imv_profile.image = pictureData.last?.imgFile
    }
    
    @IBAction func segmentChanged(_ sender: Any) {
        let height = UIScreen.main.bounds.height * 0.045
        if segMent.selectedSegmentIndex == 1{
            self.editInitAll()
            categoriesView.isHidden = true
            edtiTopcons.constant += height
            editgroupBottomCons.constant -= height
            self.imv_profile.image = UIImage.init(named: "audience.png")
        }
        else{
            self.editInitAll()
            categoriesView.isHidden = false
            edtiTopcons.constant -= height
            editgroupBottomCons.constant += height
            self.imv_profile.image = UIImage.init(named: "broadcaster.png")
        }
    }
    
    func loadLayout() {
        edtPwd.text = "123"
        confirmPwd.text = "123"
        edtName.text = "Teddy"
        edtEmail.text = "teddy@gmail.com"
        edtphoneNumber.text = "15546775782"
    }
    
    func editInit() {
        let place_email = NSLocalizedString("Email", comment: "")
        let place_password = NSLocalizedString("Password", comment: "")
        let place_username = NSLocalizedString("User Name", comment: "")
        let place_confirmpassword = NSLocalizedString("Confirm Password", comment: "")
        let place_phonenumber = NSLocalizedString("Input Phone Number", comment: "")
        
        setEdtPlaceholderColor(edtEmail, placeholderText: place_email, placeColor: UIColor.white)
        setEdtPlaceholderColor(edtPwd, placeholderText: place_password, placeColor: UIColor.white)
        setEdtPlaceholderColor(edtName, placeholderText: place_username, placeColor: UIColor.white)
        setEdtPlaceholderColor(confirmPwd, placeholderText: place_confirmpassword, placeColor: UIColor.white)
        setEdtPlaceholderColor(edtphoneNumber, placeholderText: place_phonenumber, placeColor: UIColor.white)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        createGradientView(signBtnView)
    }
    
    @IBAction func gotoLogin(_ sender: Any) {
        gotoVC("LoginVC")
    }
    
    func editInitAll() {
       edtEmail.text = ""
       edtphoneNumber.text = ""
       edtName.text = ""
       edtPwd.text = ""
       confirmPwd.text = ""
       categoriesLabel.text = ""
       categoriType = ""
    }
    
    @IBAction func signUpBtnClicked(_ sender: Any) {
        print("signup")
        if !socialLogin{
            validator.registerField(edtName, errorLabel: nil , rules: [RequiredRule()])
            validator.registerField(edtEmail, errorLabel: nil , rules: [RequiredRule(),EmailRule()])
            validator.registerField(edtPwd, errorLabel: nil , rules: [RequiredRule(),AlphaNumericRule()])
             validator.registerField(edtphoneNumber, errorLabel: nil , rules: [RequiredRule(),MinLengthRule(length: 11)])
            validator.registerField(confirmPwd, errorLabel: nil , rules: [RequiredRule(),ConfirmationRule(confirmField: edtPwd)])
            validator.styleTransformers(success:{ (validationRule) -> Void in
                
                validationRule.errorLabel?.isHidden = true
                validationRule.errorLabel?.text = ""
                
            }, error:{ (validationError) -> Void in
                
                let toast = NSLocalizedString("Please input all information correctly.\n Phone number must be 12 digits.", comment: "")
                self.alertDisplay(alertController: self.alertMake(toast))
                
            })
            validator.validate(self)
        }
        else if socialLogin{
            self.signupwithWECHAT()
        }
        else{
            return
        }
    }
    
    func validationSuccessful() {
        print("here")
        if segMent.selectedSegmentIndex == 0{
            if self.categoriesLabel.text == ""{
                let toast = NSLocalizedString("Please select categories that you want!", comment: "")
                self.alertDisplay(alertController: self.alertMake(toast))
                
            }
            else{
                categories = self.categoriesLabel.text
                signupApi(email :edtEmail.text!, phone: edtphoneNumber.text!, username : edtName.text!, password : edtPwd.text!,type: "teacher",categorie : categories!)
            }
        }
        else{
            if segMent.selectedSegmentIndex == 0{
                segState = "teacher"
            }
            else{
                segState = "student"
            }

            if self.categoriesLabel.text == ""{
                categories = ""
            }
            else{
                categories = self.categoriesLabel.text
            }
            signupApi(email :edtEmail.text!, phone: edtphoneNumber.text!, username : edtName.text!, password : edtPwd.text!,type: segState!,categorie : categories!)
        }
    }
    
    func signupApi( email : String, phone: String,username : String, password : String,type:String,categorie:String ){
        let toast = NSLocalizedString("Connecting... \n Just a moment!", comment: "")
        self.showProgressSet_withMessage(toast, msgOn: false, styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
        
        ApiManager.signup( email: email,phone: phone,username: username,password: password,type: type,categorie: categorie, images: imageFils) { (isSuccess, data) in
            self.hideProgress()
            
            if isSuccess{
                let result_message = data as! String
                if(result_message == "0"){
                    if self.socialLogin{
                        UserDefault.setBool(key: PARAMS.SOCIAL_LOGIN, value: true)
                    }
                    UserDefault.setString(key: PARAMS.EMAIL, value: email)
                    self.signupsuccess(email)
                }
                else {
                    print("Network Error!")
                    let toast = NSLocalizedString("Please select user photo!", comment: "")
                    self.alertDisplay(alertController: self.alertMake(toast))
                }
            }
            else{
                if data == nil{
                    self.alertDisplay(alertController: self.alertMake("Connection Error!"))
                    print("connection Error")
                }
                else{
                    let result_message = data as! String
                    if(result_message == "1"){
                        let toast = NSLocalizedString("Email already Exist!", comment: "")
                        self.alertDisplay(alertController: self.alertMake(toast))
                        
                    }
                    
                    else{
                        self.alertDisplay(alertController: self.alertMake("Network Error!"))
                    }
                }
            }
        }
    }
    
    func signupsuccess(_ email : String){
        ApiManager.getMyDetail(email: email){
            (isSuceess, data) in
            if isSuceess{
                let dict = JSON(data as Any)
                user = UserModel(dict)
                user?.saveUserInfo()
                UserDefault.setBool(key: Constants.LOGOUT, value: false)
                if user!.isValid{
                    self.signUpSuccess_gotoMain()
                }
                else{
                    return
                }
            }
        }
    }
    
    func signUpSuccess_gotoMain()  {
        
        edtEmail.text = ""
        edtphoneNumber.text = ""
        edtName.text = ""
        edtPwd.text = ""
        confirmPwd.text = ""
        categoriesLabel.text = ""
        categoriType = ""
        
        AppDelegate.setupReferenceUITabBarController()
        AppDelegate.setupHHTabBarView()
        mainNav = NavigationController(rootViewController: AppDelegate.referenceUITabBarController)
        mainNav.modalPresentationStyle = .fullScreen
        self.present(mainNav, animated: true, completion: nil)
        mainNav.isNavigationBarHidden = true
        mainNav.navigationBar.setBackgroundImage(UIImage(), for: .default)
        mainNav.navigationBar.shadowImage = UIImage()
        mainNav.navigationBar.isTranslucent = true
        mainNav.view.backgroundColor = .clear
    }
    
    @IBAction func categoriesBtnClicked(_ sender: Any) {
        self.gotoNavPresent1("CategoriesVC")
    }
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        print("validation error")
    }
    
    @IBAction func signUpwithWechat(_ sender: Any) {
        //TODO : we must implement wechat login
        print("wechat signup")
        self.signupwithWECHAT()
    }
    
    func signupwithWECHAT()  {
        wechatRequest = 2
        let req = SendAuthReq()
        req.scope = "snsapi_userinfo" //Important that this is the same
        req.state = "com.medi.edulive.wechat_login" //This can be any random value
        if !WXApi.send(req) {
            showToast(NSLocalizedString("Please Installwechat", comment: ""))
        }
    }
    
    @objc func onWechatSuccess1(_ notification : Notification) {
        
        print("reached login wechat success")
        if let data = notification.userInfo as? [String: String] {
            if let authcode = data["response"] {
                getWechatToken(authcode: authcode)
            }
        }
    }
    
    @objc func onWechatFail1(_ notifcaion : Notification) {
        showToast(NSLocalizedString("error", comment: ""))
    }
    
    func getWechatToken(authcode : String) {
            
            //showHUD()
    //        self.showProgressSet_withMessage("Connecting... \n Just a moment!", msgOn: true, styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
            
            let URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=\(Constants.WECHAT_ID)&secret=\(Constants.WECHAT_SECRET)&code=\(authcode)&grant_type=authorization_code"
            
            
            Alamofire.request(URL, method:.get)
                .responseJSON { response in
                    
                    switch response.result {
                        
                    case .failure:
                        self.showToast(NSLocalizedString("connection_faild", comment: ""))
                        
                        
                    case .success(let data):
                        
                        let dict = JSON(data)
                        print(dict)
                        let token = dict["access_token"].stringValue
                        if let openId = dict["openid"].string, !openId.isEmpty {
                        
                            self.getWhchatInfo(token: token, openId: openId)
                            //let accountId = "wx_\(openId)";
                            //self.socialLogin(accountId: accountId, type: 1)
                        }
                    }
                    
            }
        }
    func getWhchatInfo(token : String, openId : String) {
        
        let URL = "https://api.weixin.qq.com/sns/userinfo?access_token=\(token)&openid=\(openId)"
        
        
        Alamofire.request(URL, method:.get)
            .responseJSON { response in
                
                switch response.result {
                    
                case .failure:
                    self.showToast(NSLocalizedString("connection_faild", comment: ""))
                    self.hideProgress()
                    
                case .success(let data):
                    
                    _ = "wx_"
                    let dict = JSON(data)
                    print(dict)
                    
                    let city = dict["city"].stringValue
                    let unionid = dict["unionid"].stringValue
                    _ = dict["headimgurl"].stringValue
                    let openid = dict["openid"].stringValue
                    var username = ""
                    if let nickname = dict["nickname"].string {
                         username = nickname
                    } else {
                         username = city
                    }
                    self.socialSignUp(username: username, union_id: unionid, open_id: openid)
                }
        }
    }
    
    func socialSignUp(username : String,  union_id : String, open_id : String) {
        socialLogin = true
        if segMent.selectedSegmentIndex == 0{
            if categoriesLabel.text == ""{
                let toast = NSLocalizedString("Please Select Categories and User Photo!", comment: "")
                self.alertDisplay(alertController: self.alertMake(toast))
            }
            else{
                self.signupApi(email: union_id, phone: "wechatuser", username: username, password: open_id, type: "teacher", categorie: self.categoriesLabel.text ?? "")
            }
        }
        else{
            self.signupApi(email: union_id, phone: "wechatuser", username: username, password: open_id, type: "student", categorie: "")
        }
    }
}

extension SignUpVC : ImagePickerDelegate { // custom uiImagePicker
    
    func didSelect(image: UIImage?) {
        self.pictureData.removeAll()
        if let image = image {
        let one = CapturePicModel(image) // data add
        pictureData.append(one) // append action
        self.gotoUploadProfile()
            
        }
        else{
            print("No picture")
        }
    }
}
class CapturePicModel {
    
    var imgFile : UIImage?
    var imgUrl : String?
    
    init(_ imgFile : UIImage) {
        self.imgFile = imgFile
    }
    
    init(imgUrl : String) {
        self.imgUrl = imgUrl
    }
}
