//
//  MainTeacher.swift
//  Edulive
//
//  Created by Mac on 2/25/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import SwipeCellKit
import SwiftyJSON
import SwiftyUserDefaults

class MainTeacher : BaseVC1 {
    
    @IBOutlet weak var ui_collectionView: UICollectionView!
    @IBOutlet weak var col_myLesson: UICollectionView!

    var photoHeight : Int = 0
    let slider = UISlider()
    var amount = 100
    var bool_selected = false
    var dst_myLesson = [ContentModel1]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        networkcondition1 = 1
        contentDataSource.removeAll()
        teacherLessonDataSource.removeAll()
        self.bool_selected = false
        //self.getMyStudents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        global_amount = 100
        self.getmyLessonData()
    }
    @IBAction func topViewAllBtnClicked(_ sender: Any) {
        self.gotoNavPresent1("CreatLessonsViewall")
    }
    @IBAction func bottomViewAllBtnClicked(_ sender: Any) {
        self.gotoNavPresent1("MyLessonsViewall")
    }
}

extension MainTeacher : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == ui_collectionView{
            return subDataSource.count
        }
        else{
            return dst_myLesson.count
        }
    }
    
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        if collectionView == ui_collectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionCell
                cell.entity = subDataSource[indexPath.row]
                return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myLessonCell", for: indexPath) as! myLessonCell
            cell.entity = self.dst_myLesson[indexPath.row]
                return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        contentDataSource.removeAll()
        teacherLessonDataSource.removeAll()
        //print("clicked")
        
        if collectionView == ui_collectionView{
            
            let teacherCategories = user!.category
            var categoriArr = [String]()
            categoriArr = teacherCategories?.components(separatedBy: ",") ?? [""]
                
            if categoriArr.count != 0 && categoriArr.first != "" {
                var toploop = 0
                for one in categoriArr{
                    toploop += 1
                    if one == subDataSource[indexPath.row].subName{
                        self.bool_selected = true
                        let toast = NSLocalizedString("Loading your Lessons!\n Please wait...", comment: "")
                        
                        showProgressSet_withMessage(toast, msgOn: true, styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
                        subjectCaption = subDataSource[indexPath.row].subName
                        subIndex = subDataSource[indexPath.row].id.toInt()!
                        
                        DispatchQueue.main.async(execute:  {
                            ApiManager.getMyContents(user_id: user!.id, categorie_id: subIndex){
                               (isSuccess, data) in
                                
                               if isSuccess{
                                
                                   self.hideProgress()
                                   let contentList = JSON(data as Any)
                                   var count = 0
                                    if contentList.count == 0{
                                        self.gotoNavPresent1("CreateLessonView")
                                    }
                                    else{
                                        teacherLessonDataSource.removeAll()
                                        for one in contentList{
                                            let jsoncontenlist = JSON(one.1)
                                            teacherLessonDataSource.append(LessonModel(jsoncontenlist))
                                            count += 1
                                            if count == contentList.count{
                                                self.gotoNavPresent1("CreateLessonView")
                                            }
                                        }
                                    }
                               }
                           }
                        })
                    }
                    else{
                        
                    }
                    if toploop == categoriArr.count{
                        self.gotoCheck(bool_selected)
                    }
                }
            }
                
            else{
                 self.alertDisplay(alertController: self.alertMake("You didn't enroll any categories"))
            }
        }
        else{
            let toast = NSLocalizedString("Loading your Lessons!\n Please wait...", comment: "")
            
            showProgressSet_withMessage(toast, msgOn: true, styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
            subjectCaption = self.dst_myLesson[indexPath.row].categorie
            subIndex = self.dst_myLesson[indexPath.row].categorie_id.toInt()!
            let lesson_id = self.dst_myLesson[indexPath.row].id
            var lessonDataSource = [LessonModel]()
            
            DispatchQueue.main.async(execute:  {
                ApiManager.getMyContents(user_id: user!.id, categorie_id: subIndex){
                   (isSuccess, data) in
                    
                   if isSuccess{
                    
                       self.hideProgress()
                       let contentList = JSON(data as Any)
                       var count = 0
                        if contentList.count == 0{
                            //self.showToast("There is no any lessons.Please create lessons.")
                            self.gotoNavPresent1("CreateLessonView")
                        }
                        else{
                            teacherLessonDataSource.removeAll()
                            lessonDataSource.removeAll()
                            for one in contentList{
                                let jsoncontenlist = JSON(one.1)
                                lessonDataSource.append(LessonModel(jsoncontenlist))
                                count += 1
                                if count == contentList.count{
                                    for one in lessonDataSource{
                                        if lesson_id == one.id{
                                            teacherLessonDataSource.append(one)
                                            break
                                        }
                                    }
                                    self.gotoNavPresent1("CreateLessonView")
                                }
                            }
                        }
                   }
               }
            })

        }
    }
    
    func getSubIDfromCategorieName(_ textString: String) -> Int {
        var id : Int = 0
        for one in subDataSource{
            if textString == one.subName{
                id = one.id.toInt()!
            }
        }
        return id
    }
    
    func gotoCheck(_ selectedState : Bool)  {
        if !selectedState{
            let toast = NSLocalizedString("You didn't enroll to this category! \n Please register!", comment: "")
            self.alertDisplay(alertController: self.alertMake(toast))
            self.bool_selected = false
        }
        else{
           self.bool_selected = false
        }
    }
    
    func getmyLessonData(){
        let toast = NSLocalizedString("Connecting... \n Just a moment!", comment: "")
        self.showProgressSet_withMessage(toast, msgOn: false, styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
        ApiManager.getMyAllContents(user_id: user!.id){
            (isSuccess, data) in
            self.hideProgress()
            if isSuccess{
                self.hideProgress()
                let contentList = JSON(data as Any)
                var count = 0
                 self.dst_myLesson.removeAll()
                
                if contentList.count == 0{
                    self.alertDisplay(alertController: self.alertMake("There is no any lessons for this teahcer!"))
                }
                else{
                    for one in contentList{
                        let jsoncontenlist = JSON(one.1)
                        self.dst_myLesson.append(ContentModel1(jsoncontenlist))
                        count += 1
                        if count == contentList.count{
                            self.col_myLesson.reloadData()
                        }
                    }
                }
            }
        }
    }
}

extension MainTeacher: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == ui_collectionView{
            let w = collectionView.frame.size.width / 3
            let h: CGFloat = w * 1.3
            return CGSize(width: w, height: h)
        }
        else{
            let w = collectionView.frame.size.width / 3
            let h: CGFloat = collectionView.frame.size.height
            return CGSize(width: w, height: h)
        }
    }
}

