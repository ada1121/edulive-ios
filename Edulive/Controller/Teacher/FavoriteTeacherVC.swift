
//
//  FavoriteTeacherVC.swift
//  Edulive
//
//  Created by Mac on 2/26/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import SwipeCellKit
import SwiftyJSON
import SwiftyUserDefaults

class FavoriteTeacherVC: BaseVC1 {

    @IBOutlet weak var ui_collectionView: UICollectionView!
    var studentDataSource = [StudentModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
     override func viewWillAppear(_ animated: Bool) {
        self.getMyStudents()
     }
    
    func getMyStudents() {
        studentDataSource.removeAll()
        let toast = NSLocalizedString("Connecting... \n Just a moment!", comment: "")
        self.showProgressSet_withMessage(toast, msgOn: false, styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
        ApiManager.getMyStudents(user_id: user!.id ?? 0){
            (isSuccess, data) in
            self.hideProgress()
            if isSuccess{
                let student_List = JSON(data as Any)
                if student_List.count == 0{
                    self.alertDisplay(alertController: self.alertMake("There is no students yet for your lessons!"))
                }
                else{
                    var count = 0
                    for one in student_List{
                        let jsoncontenlist = JSON(one.1)
                        self.studentDataSource.append(StudentModel(jsondata: jsoncontenlist))
                        count += 1
                        if count == jsoncontenlist.count{
                            self.ui_collectionView.reloadData()
                        }
                    }
                }
            }
        }
    }
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}

extension FavoriteTeacherVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return studentDataSource.count
    }
    
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StudentCell", for: indexPath) as! StudentCell
        
        cell.entity = studentDataSource[indexPath.row]
    
        //cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("clicked")
        
        
//        gotoMessageSendVC()
    }
}

extension FavoriteTeacherVC: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        let w = collectionView.frame.size.width / 2.1
        let h = w
        return CGSize(width: w, height: h)
        
    }
}

//extension FavoriteTeacherVC : SwipeCollectionViewCellDelegate {
//
//    func collectionView(_ collectionView: UICollectionView, editActionsForItemAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
//        guard orientation == .right else {
//           return nil
//        }
//        //let requestId = datasource[indexPath.row].requestFriendId
//
//        let removeAction = SwipeAction(style: .default, title: nil) { (action, indexPath) in
//            favoriteDataSource.remove(at: indexPath.row)
//            self.ui_collectionView.reloadData()
//        }
//
//        removeAction.transitionDelegate = ScaleTransition(duration: 0.15, initialScale: 0.7, threshold: 0.8)
//
//
//        removeAction.image = UIImage(named: "delete")
//
//        removeAction.backgroundColor = UIColor.init(named: "textColor")
//
//        removeAction.title = "Remove"
//
//        removeAction.textColor = UIColor.lightGray
//
//        return [removeAction]
//
//    }
//}


