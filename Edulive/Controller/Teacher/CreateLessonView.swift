//
//  CreateLessonView.swift
//  Edulive
//
//  Created by Mac on 2/25/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import SwipeCellKit
import SwiftyJSON
import SwiftyUserDefaults

class CreateLessonView: BaseVC1 {
    @IBOutlet weak var topcaption: UILabel!
    @IBOutlet weak var ui_collectionView: UICollectionView!
    //var teacherlessonsDataSource = [LessonModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //teacherlessonsDataSource.removeAll()
        confRoomName = ""
        descriptionText = ""
        topcaption.text = NSLocalizedString("Create Lessons", comment: "")
        //self.initDataSource()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.ui_collectionView.reloadData()
    }
    
//    func initDataSource() {
//        teacherlessonsDataSource.removeAll()
//        for one in teacherLessonDataSource{
//            teacherlessonsDataSource.append(one)
//        }
//    }
    @IBAction func createLessons(_ sender: Any) {
        self.ui_collectionView.reloadData()
        self.gotoNavPresent1("UploadPDF")
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension CreateLessonView : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return teacherLessonDataSource.count
    }
    
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TeacherCell", for: indexPath) as! TeacherCell
        
        cell.entity = teacherLessonDataSource[indexPath.row]
    
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        print("clicked")
//        contentIndex = indexPath.row
//        descriptionText = teacherLessonDataSource[contentIndex].description
//        str_paycontentName = student_teacherlessonsDataSource[contentIndex].title
//        self.gotoNavPresent1("Description")
    }
}

extension CreateLessonView: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let h: CGFloat = 200
        let w = collectionView.frame.size.width
        return CGSize(width: w, height: h)
    }
}

extension CreateLessonView : SwipeCollectionViewCellDelegate {
    func collectionView(_ collectionView: UICollectionView, editActionsForItemAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else {
           return nil
        }
        //let requestId = datasource[indexPath.row].requestFriendId

        let videoAction = SwipeAction(style: .default, title: nil) { (action, indexPath) in
            confRoomName = teacherLessonDataSource[indexPath.row].id
            self.gotoVC("BroadCastNav") // change
        }

        let editAction = SwipeAction(style: .default, title: nil) { (action, indexPath) in
            self.ui_collectionView.reloadData()
            contentTitle = teacherLessonDataSource[indexPath.row].title
            fileURL = teacherLessonDataSource[indexPath.row].file_url
            descriptionLast = teacherLessonDataSource[indexPath.row].description
            idLast = teacherLessonDataSource[indexPath.row].id
            amountLast = teacherLessonDataSource[indexPath.row].amount
            edit = true
            self.gotoNavPresent1("UploadPDF")
        }
        
        let removeAction = SwipeAction(style: .default, title: nil) { (action, indexPath) in
            
            self.showProgress()
            ApiManager.removeMyContent(content_id: teacherLessonDataSource[indexPath.row].id.toInt() ?? 0){
                (isSuccess, data) in
                if isSuccess{
                    self.hideProgress()
                    teacherLessonDataSource.remove(at: indexPath.row)
                    self.ui_collectionView.reloadData()
                }
                else{
                    self.showToast("Network Error")
                }
            }
        }
               // customize swipe action
        videoAction.transitionDelegate = ScaleTransition(duration: 0.15, initialScale: 0.7, threshold: 0.8)
        editAction.transitionDelegate = ScaleTransition(duration: 0.15, initialScale: 0.7, threshold: 0.8)
        removeAction.transitionDelegate = ScaleTransition(duration: 0.15, initialScale: 0.7, threshold: 0.8)

               // customize the action appearance
        videoAction.image = UIImage(named: "videostreaming")
        editAction.image = UIImage(named: "edit")
        removeAction.image = UIImage(named: "delete")
        
        videoAction.backgroundColor = UIColor.init(named: "textColor")
        editAction.backgroundColor = UIColor.init(named: "textColor")
        removeAction.backgroundColor = UIColor.init(named: "textColor")
        
        videoAction.title = NSLocalizedString("Broadcast", comment: "")
        editAction.title = NSLocalizedString("Edit", comment: "")
        removeAction.title = NSLocalizedString("Remove", comment: "")
        
        videoAction.textColor = UIColor.lightGray
        editAction.textColor = UIColor.lightGray
        removeAction.textColor = UIColor.lightGray

        return [removeAction,editAction,videoAction]

    }
}

