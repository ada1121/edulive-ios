//
//  UploadPDF.swift
//  Edulive
//
//  Created by Mac on 2/24/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import PDFKit
import SwiftyJSON

class UploadPDF: BaseVC1 {
    
    var paidCheck : Bool = true
    var lessonName = ""
    var contentdescription = ""
    var filePath = ""
    var fileNameUpload = ""

    @IBOutlet weak var edtPrice: UITextField!
    @IBOutlet weak var txvDescription: UITextView!
    @IBOutlet weak var edtLessonName: UITextField!
    @IBOutlet weak var imvFreecheck: UIImageView!
    @IBOutlet weak var imvPaidCheck: UIImageView!
    @IBOutlet weak var uivUnderLine: UIView!
    
    @IBOutlet weak var pdfView: UIView!
    let pdfviewer =  PDFView()
    var pdfFile = PDFDocument()
    var pdfDocumentURL :URL?
    var lastid = ""
    
    @IBOutlet weak var buttonCaption: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        contentTitle = self.teacherlessonsDataSource[indexPath.row].title
//        fileURL = self.teacherlessonsDataSource[indexPath.row].file_url
//        descriptionLast = self.teacherlessonsDataSource[indexPath.row].description
//        idLast = self.teacherlessonsDataSource[indexPath.row].id
//        amountLast = self.teacherlessonsDataSource[indexPath.row].amount
        
        if edit{
            buttonCaption.text = NSLocalizedString("Update", comment: "")
            edtLessonName.text = contentTitle
            txvDescription.text = descriptionLast
            lastid = idLast
            if amountLast.toInt() ?? 0 > 0{
                paidCheck = true
                self.showPaidEdtText()
                self.edtPrice.text = amountLast
            }
            else{
                paidCheck = false
                self.showPaidEdtText()
            }
            contentTitle = ""
            descriptionLast = ""
            idLast = ""
            amountLast = ""
        }
        else{
           buttonCaption.text = NSLocalizedString("Publish", comment: "")
        }
        pdfviewer.frame = self.pdfView.bounds
        
        pdfviewer.centerX = UIScreen.main.bounds.width - 60
        pdfviewer.centerY = 68
        self.view.addSubview(pdfviewer)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeBtnClicked(_ sender: Any) {
        edit = false
        self.updateContents()
    }
    
    @IBAction func uploadBtnClicked(_ sender: Any) {
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["PDF","com.adobe.pdf"], in: .import)
        documentPicker.delegate = self
        self.present(documentPicker, animated: true)
    }
    
    @IBAction func paidBtnClicked(_ sender: Any) {
        self.paidCheck = true
        self.showPaidEdtText()
    }
    
    @IBAction func freeBtnClicked(_ sender: Any) {
        self.paidCheck = false
        self.showPaidEdtText()
    }
    
    @IBAction func publishBtnClicked(_ sender: Any) {
        if !edit{
            lessonName = edtLessonName.text ?? ""
            contentdescription = txvDescription.text
            var price = 0
            if lessonName == "" || contentdescription == ""{
                self.alertDisplay(alertController: self.alertMake("Please Input Lesson Name and Description!"))
                return
            }
            if paidCheck{
                price = self.edtPrice.text!.toInt() ?? 0
            }
            else{
                price = 0
            }
            if paidCheck && price == 0{
                self.alertDisplay(alertController: self.alertMake("Input Price!"))
            }
            else{
                showProgressSet_withMessage("Publishing Content... \n Just a moment!", msgOn: true, styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
                ApiManager.uploadContent(user_id: "\(user!.id ?? 0)", categorie_id: "\(subIndex)", title: lessonName, amount: "\(price)", description: contentdescription, file: fileNameUpload){
                    (isSuccess, data) in
                    self.hideProgress()
                    if isSuccess{
                        let status = data as! String
                        if status == "0"{
                            let alert = UIAlertController(title: "", message: "UploadSuccess", preferredStyle: .alert)
                             
                            let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
                             self.setInit()
                            })
                            alert.addAction(ok)
                            self.present(alert, animated: true)
                            
                        }
                        else if status == "1"{
                             self.alertDisplay(alertController: self.alertMake("File Name Aleady Exist!"))
                        }
                        if status == "2"{
                             self.alertDisplay(alertController: self.alertMake("Not pdf document!"))
                        }
                        else{
                             self.alertDisplay(alertController: self.alertMake("Connection Fail!"))
                        }
                    }
                    else{
                        self.alertDisplay(alertController: self.alertMake("Network Error!"))
                    }
                }
            }
        }
        else{
            lessonName = edtLessonName.text ?? ""
            contentdescription = txvDescription.text
            var price = 0
            if lessonName == "" || contentdescription == ""{
                self.showToast("Please Input Lesson Name and Description!")
                return
            }
            if paidCheck{
                price = self.edtPrice.text!.toInt() ?? 0
            }
            else{
                price = 0
            }
            if paidCheck && price == 0{
                self.alertDisplay(alertController: self.alertMake("Input Price!"))
            }
            else{
                showProgressSet_withMessage("Updating Content... \n Just a moment!", msgOn: true, styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
                ApiManager.updateContent(content_id: lastid, title: lessonName, amount: "\(price)", description: contentdescription, file: fileNameUpload){
                    (isSuccess, data) in
                    self.hideProgress()
                    
                    if isSuccess{
                        let status = data as! String
                        if status == "0"{
                             self.alertDisplay(alertController: self.alertMake("UpdateSuccess!"))
                        }
                        else if status == "1"{
                             self.alertDisplay(alertController: self.alertMake("File Name Aleady Exist!"))
                        }
                        if status == "2"{
                             self.alertDisplay(alertController: self.alertMake("Not pdf document!"))
                        }
                        else{
                             self.alertDisplay(alertController: self.alertMake("Connection Fail!"))
                        }
                    }
                    else{
                        self.alertDisplay(alertController: self.alertMake("Network Error!"))
                    }
                }
            }
        }
    }
    
    func setInit()  {
        self.paidCheck = true
        self.lessonName = ""
        self.contentdescription = ""
        self.filePath = ""
        self.fileNameUpload = ""
        edtLessonName.text = ""
        txvDescription.text = ""
        edtPrice.text = ""
    }
    
    func updateContents()  {
        
        let toast = NSLocalizedString("Loading your Lessons!\n Please wait...", comment: "")
        
        showProgressSet_withMessage(toast, msgOn: true, styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
        
        
        DispatchQueue.main.async(execute:  {
            ApiManager.getMyContents(user_id: user!.id, categorie_id: subIndex){
               (isSuccess, data) in
                
               if isSuccess{
                
                   self.hideProgress()
                   let contentList = JSON(data as Any)
                        teacherLessonDataSource.removeAll()
                   var count = 0
                    for one in contentList{
                        let jsoncontenlist = JSON(one.1)
                        teacherLessonDataSource.append(LessonModel(jsoncontenlist))
                        count += 1
                        if count == contentList.count{
                            self.navigationController?.popViewController(animated: false)
                        }
                    }
               }
           }
        })
    }
    
    func showPaidEdtText() {
        if paidCheck{
            self.imvPaidCheck.isHidden = false
            self.edtPrice.isHidden = false
            self.edtPrice.becomeFirstResponder()
            self.uivUnderLine.isHidden = false
            self.imvFreecheck.isHidden = true
        }
        else{
            self.imvPaidCheck.isHidden = true
            self.edtPrice.isHidden = true
            self.uivUnderLine.isHidden = true
            self.imvFreecheck.isHidden = false
        }
    }
    
    func createPdfURL() -> URL {

        let fileName = randomString(length: 5) + ".pdf"
        fileNameUpload = fileName
        let documentsDirectories = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = documentsDirectories.first!
        let pdfPageURL = documentDirectory.appendingPathComponent("\(fileName)")

        return pdfPageURL
    }
    
    func savePdfToLocaFile(pdf: PDFDocument) -> Void {

        // CREATE URL FOR PDF DOCUMENT
        let pdfURL = createPdfURL()
        print("PDF SAVED TO URL: \(pdfURL)")

        self.pdfDocumentURL = pdfURL

        pdf.write(to: pdfURL)
    }
}

//MARK: - UIDocumentPickerDelegate
extension UploadPDF: UIDocumentPickerDelegate  {
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard urls.count != 0 else { return }
        filePath = urls[0].absoluteString
            print(urls[0].absoluteString + " --- " + urls[0].absoluteURL.absoluteString)
        guard let pdfURL = URL.init(string: filePath) else{
            print("no pdf URL")
            return

        }
        guard let pdf = PDFDocument.init(url: pdfURL) else{
            print("NO PDF DOCUMENT FOUND")
            return

        }
        self.pdfviewer.document = pdf
        
//        pdfPreview.document = pdf // CAN VIEW PDF FILE!!!
        pdfviewer.autoScales = true
        self.savePdfToLocaFile(pdf: pdf)
    }
}
