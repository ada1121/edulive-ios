

import UIKit
import SwipeCellKit
import SwiftyJSON
import SwiftyUserDefaults
import Alamofire
import SwiftyXMLParser
import CryptoKit
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG

class MyLessonsViewall : BaseVC1 {

    @IBOutlet weak var ui_collectionView: UICollectionView!
   
    var contentListDataSource = [ContentModel1]()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        self.getmyLessonData()
    }
    
    override func viewDidLayoutSubviews() {
        //self.ui_collectionView.reloadData()
    }
    
    func getmyLessonData(){
       let toast = NSLocalizedString("Connecting... \n Just a moment!", comment: "")
        self.showProgressSet_withMessage(toast, msgOn: false, styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
        ApiManager.getMyAllContents(user_id: user!.id){
            (isSuccess, data) in
            self.hideProgress()
            if isSuccess{
                self.hideProgress()
                let contentList = JSON(data as Any)
                var count = 0
                 self.contentListDataSource.removeAll()
                
                if contentList.count == 0{
                    self.alertDisplay(alertController: self.alertMake("There is no any lessons for this teahcer!"))
                }
                else{
                    for one in contentList{
                        let jsoncontenlist = JSON(one.1)
                        self.contentListDataSource.append(ContentModel1(jsoncontenlist))
                        count += 1
                        if count == contentList.count{
                            self.ui_collectionView.reloadData()
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
      // for the back button click event
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getSubIDfromCategorieName(_ textString: String) -> Int {
        var id : Int = 0
        for one in subDataSource{
            if textString == one.subName{
                id = one.id.toInt()!
            }
        }
        return id
    }
}

    extension MyLessonsViewall : UICollectionViewDelegate, UICollectionViewDataSource{
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return self.contentListDataSource.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContentCell1", for: indexPath) as! ContentCell1
            cell.entity = self.contentListDataSource[indexPath.row]
            return cell
        }
        
        
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            let toast = NSLocalizedString("Loading your Lessons!\n Please wait...", comment: "")
            
            showProgressSet_withMessage(toast, msgOn: true, styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
            subjectCaption = self.contentListDataSource[indexPath.row].categorie
            subIndex = self.contentListDataSource[indexPath.row].categorie_id.toInt()!
            let lesson_id = self.contentListDataSource[indexPath.row].id
            var lessonDataSource = [LessonModel]()
            
            DispatchQueue.main.async(execute:  {
                ApiManager.getMyContents(user_id: user!.id, categorie_id: subIndex){
                   (isSuccess, data) in
                    
                   if isSuccess{
                    
                       self.hideProgress()
                       let contentList = JSON(data as Any)
                       var count = 0
                        if contentList.count == 0{
                            //self.showToast("There is no any lessons.Please create lessons.")
                            self.gotoNavPresent1("CreateLessonView")
                        }
                        else{
                            teacherLessonDataSource.removeAll()
                            lessonDataSource.removeAll()
                            for one in contentList{
                                let jsoncontenlist = JSON(one.1)
                                lessonDataSource.append(LessonModel(jsoncontenlist))
                                count += 1
                                if count == contentList.count{
                                    for one in lessonDataSource{
                                        if lesson_id == one.id{
                                            teacherLessonDataSource.append(one)
                                            break
                                        }
                                    }
                                    self.gotoNavPresent1("CreateLessonView")
                                }
                            }
                        }
                   }
               }
            })
        }
    }

    extension MyLessonsViewall: UICollectionViewDelegateFlowLayout{
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
            let w = collectionView.frame.size.width / 2.1
            let h = w
            return CGSize(width: w, height: h)
            
        }
    }

   


