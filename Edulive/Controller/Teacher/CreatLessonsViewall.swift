
//
//  CreatLessonsViewall.swift
//  Edulive
//
//  Created by Mac on 3/14/20.
//  Copyright © 2020 Mac. All rights reserved.
//


import UIKit
import SwipeCellKit
import SwiftyJSON
import SwiftyUserDefaults
import Alamofire
import SwiftyXMLParser
import CryptoKit
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG

class CreatLessonsViewall : BaseVC1 {

    @IBOutlet weak var ui_collectionView: UICollectionView!
   var bool_selected = false
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
    }
    
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    func gotoCheck(_ selectedState : Bool)  {
        if !selectedState{
            let toast = NSLocalizedString("You didn't enroll to this category! \n Please register!", comment: "")
            self.alertDisplay(alertController: self.alertMake(toast))
            self.bool_selected = false
        }
        else{
           self.bool_selected = false
        }
    }
}

    extension CreatLessonsViewall : UICollectionViewDelegate, UICollectionViewDataSource{
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return subDataSource.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionCell
            cell.entity = subDataSource[indexPath.row]
            return cell
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            let teacherCategories = user!.category
            var categoriArr = [String]()
            categoriArr = teacherCategories?.components(separatedBy: ",") ?? [""]
                
            if categoriArr.count != 0 && categoriArr.first != "" {
                var toploop = 0
                for one in categoriArr{
                    toploop += 1
                    if one == subDataSource[indexPath.row].subName{
                        self.bool_selected = true
                        let toast = NSLocalizedString("Loading your Lessons!\n Please wait...", comment: "")
                        
                        showProgressSet_withMessage(toast, msgOn: true, styleVal: 2, backColor: UIColor.init(named: "btnColor")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
                        subjectCaption = subDataSource[indexPath.row].subName
                        subIndex = subDataSource[indexPath.row].id.toInt()!
                        
                        DispatchQueue.main.async(execute:  {
                            ApiManager.getMyContents(user_id: user!.id, categorie_id: subIndex){
                               (isSuccess, data) in
                                
                               if isSuccess{
                                
                                   self.hideProgress()
                                   let contentList = JSON(data as Any)
                                   var count = 0
                                    if contentList.count == 0{
                                        teacherLessonDataSource.removeAll()
                                        //self.showToast("There is no any lessons.Please create lessons.")
                                        self.gotoNavPresent1("CreateLessonView")
                                    }
                                    else{
                                        teacherLessonDataSource.removeAll()
                                        for one in contentList{
                                            let jsoncontenlist = JSON(one.1)
                                            teacherLessonDataSource.append(LessonModel(jsoncontenlist))
                                            count += 1
                                            if count == contentList.count{
                                                self.gotoNavPresent1("CreateLessonView")
                                            }
                                        }
                                    }
                               }
                           }
                        })
                    }
                    else{
                        
                    }
                    if toploop == categoriArr.count{
                        self.gotoCheck(bool_selected)
                    }
                }
            }
                
            else{
                 self.alertDisplay(alertController: self.alertMake("You didn't enroll any categories"))
            }
        }
    }

    extension CreatLessonsViewall : UICollectionViewDelegateFlowLayout{
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
            let w = collectionView.frame.size.width / 2.1
            let h = w
            return CGSize(width: w, height: h)
        }
    }



