//
//  TotalUserModel.swift
//  Edulive
//
//  Created by Mac on 2/26/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import SwiftyJSON

class TotalUserModel {
    
    var id =  ""
    var username =  ""
    var photo =  ""
    var email =  ""
    var phone =  ""
    var type =  ""
    var categorie =  ""
    var recommended =  ""
    
    init(_ content : JSON ) {
        self.id = content[PARAMS.ID].stringValue
        self.username = content[PARAMS.USERNAME].stringValue
        self.photo = content[PARAMS.PHOTO].stringValue
        self.email = content[PARAMS.EMAIL].stringValue
        self.phone = content[PARAMS.PHONE].stringValue
        self.type = content[PARAMS.TYPE].stringValue
        self.categorie = content[PARAMS.CATEGORIE].stringValue
        self.recommended = content[PARAMS.RECOMMENDED].stringValue
    }
}
