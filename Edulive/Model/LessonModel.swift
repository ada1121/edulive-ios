//
//  LessonModel.swift
//  Edulive
//
//  Created by Mac on 2/27/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import SwiftyJSON

class LessonModel {
    
    var id =  ""
    var title =  ""
    var description = ""
    var amount =  ""
    var file_url =  ""
    var paid_student = 0
   
    init(_ content : JSON ) {
        self.id = content[PARAMS.ID].stringValue
        self.title = content[PARAMS.TITLE].stringValue
        self.amount = content[PARAMS.AMOUNT].stringValue
        self.file_url = content[PARAMS.FILE_URL].stringValue
        self.description = content[PARAMS.DESCTIPTION].stringValue
        self.paid_student = content["paid_student"].intValue
    }
}
