//
//  studentModel.swift
//  Edulive
//
//  Created by Mac on 2/28/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import SwiftyJSON
class StudentModel{
    
    var user_id = 0
    var email = ""
    var username = ""
    var phone = ""
    var photo = ""
    init(jsondata : JSON) {
        self.user_id = jsondata[PARAMS.ID].intValue
        self.email = jsondata[PARAMS.EMAIL].stringValue
        self.username = jsondata[PARAMS.USERNAME].stringValue
        self.phone = jsondata[PARAMS.PHONE].stringValue
        self.photo = jsondata[PARAMS.PHOTO].stringValue
    }
}
