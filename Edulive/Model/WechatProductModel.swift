//
//  WechatProductModel.swift
//  Edulive
//
//  Created by Mac on 3/3/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
class WechatProductModel {
    var appid = ""
    var mch_id = ""
    var nonce_str = ""
    var sign = ""
    var body = ""
    var out_trade_no = ""
    var total_fee = 0
    var spbill_create_ip = ""
    var notify_url = ""
    var trade_type = ""
    var attach = ""
    
    init(appid : String,mch_id: String,nonce_str : String,sign: String,body:String,out_trade_no: String,total_fee: Int,spbill_create_ip : String,notify_url: String,trade_type: String,attach: String) {
        self.appid = appid
        self.mch_id = mch_id
        self.nonce_str = nonce_str
        self.sign = sign
        self.body = body
        self.out_trade_no = out_trade_no
        self.total_fee = total_fee
        self.spbill_create_ip = spbill_create_ip
        self.notify_url = notify_url
        self.trade_type = trade_type
        self.attach = attach
    }
    
    func toDictionary() -> [String : Any] {

        var dictionary = [String:Any]()
        let otherSelf = Mirror(reflecting: self)
        for child in otherSelf.children {
            if let key = child.label {
                dictionary[key] = child.value
            }
        }
        print("WechatProductModel To Dicntionary :: \(dictionary.description)")
        return dictionary
    }
    
    func toString() -> String {
        
        var returnedString = ""
        returnedString = "appid=" + appid  + "&" + "attach=" + attach + "&" + "body=" + body + "&" + "mch_id=" + mch_id  + "&" + "nonce_str=" + nonce_str + "&" + "notify_url=" + notify_url + "&" + "out_trade_no=" + out_trade_no + "&" + "spbill_create_ip=" + spbill_create_ip + "&" + "total_fee=" + "\(total_fee)" + "&" + "trade_type=" + trade_type + "&"
        return returnedString
    }
}
