//
//  UserModel.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 1/18/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserModel: NSObject {
        var id: Int!
        var email: String?
        var username: String?
        var phone: String?
        var type: String?
        var category: String?
        var photo : String?
    
       override init() {
           super.init()
           id = 0
           email = ""
           username = ""
           phone = ""
           type = ""
           category = ""
           photo = ""
       }
       
    init(_ json : JSON) {
        
        self.id = json[PARAMS.USERID].stringValue.toInt()
        self.email = json[PARAMS.EMAIL].stringValue
        self.username = json[PARAMS.USERNAME].stringValue
        self.phone = json[PARAMS.PHONE].stringValue
        self.type = json[PARAMS.TYPE].stringValue
        self.category = json[PARAMS.CATEGORIE].stringValue
        self.photo = json[PARAMS.PHOTO].stringValue
    }
    // Check and returns if user is valid user or not
   var isValid: Bool {
       return id != nil && id != 0 && email != ""
   }
    
    // Recover user credential from UserDefault
    func loadUserInfo() {
        id = UserDefault.getInt(key: PARAMS.ID, defaultValue: 0)
        email = UserDefault.getString(key: PARAMS.EMAIL, defaultValue: "")
        username = UserDefault.getString(key: PARAMS.USERNAME, defaultValue: "")
        phone = UserDefault.getString(key: PARAMS.PHONE, defaultValue: "")
        type = UserDefault.getString(key: PARAMS.TYPE, defaultValue: "")
        category = UserDefault.getString(key: PARAMS.CATEGORIE, defaultValue: "")
        photo = UserDefault.getString(key: PARAMS.PHOTO, defaultValue: "")
    }
    // Save user credential to UserDefault
    func saveUserInfo() {
        UserDefault.setInt(key: PARAMS.ID, value: id)
        UserDefault.setString(key: PARAMS.EMAIL, value: email)
        UserDefault.setString(key: PARAMS.USERNAME, value: username)
        UserDefault.setString(key: PARAMS.PHONE, value: phone)
        UserDefault.setString(key: PARAMS.TYPE, value: type)
        UserDefault.setString(key: PARAMS.CATEGORIE, value: category)
        UserDefault.setString(key: PARAMS.PHOTO, value: photo)
        
    }
    // Clear save user credential
    func clearUserInfo() {
        
        id = 0
        username = ""
        email = ""
        phone = ""
        type = ""
        category = ""
        photo = ""
        
       UserDefault.setInt(key: PARAMS.ID, value: 0)
       UserDefault.setString(key: PARAMS.EMAIL, value: nil)
       UserDefault.setString(key: PARAMS.USERNAME, value: nil)
       UserDefault.setString(key: PARAMS.PHONE, value: nil)
       UserDefault.setString(key: PARAMS.TYPE, value: nil)
       UserDefault.setString(key: PARAMS.CATEGORIE, value: nil)
       UserDefault.setString(key: PARAMS.PHOTO, value: nil)
    }
    
    func updateUserInfo(user: UserModel) {
        id = user.id
        username = user.username
        email = user.email
        phone = user.phone
        type = user.type
        category = user.category
        photo = user.photo
    }
}

