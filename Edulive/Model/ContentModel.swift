//
//  ContentModel.swift
//  Edulive
//
//  Created by Mac on 2/18/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import SwiftyJSON

class ContentModel {
    
    var id =  ""
    var title =  ""
    var amount =  ""
    var file_url =  ""
    var user_id =  ""
    var email =  ""
    var username =  ""
    var phone =  ""
    var type =  ""
    var categorie =  ""
    var photo =  ""
    var description = ""
    var favorite = ""
    
    init(_ content : JSON ) {
        
        self.id = content[PARAMS.ID].stringValue
        self.title = content[PARAMS.TITLE].stringValue
        self.amount = content[PARAMS.AMOUNT].stringValue
        self.file_url = content[PARAMS.FILE_URL].stringValue
        self.user_id = content[PARAMS.USERID].stringValue
        self.email = content[PARAMS.EMAIL].stringValue
        self.username = content[PARAMS.USERNAME].stringValue
        self.phone = content[PARAMS.PHONE].stringValue
        self.type = content[PARAMS.TYPE].stringValue
        self.categorie = content[PARAMS.CATEGORIE].stringValue
        self.photo = content[PARAMS.PHOTO].stringValue
        self.description = content[PARAMS.DESCTIPTION].stringValue
        self.favorite = content[PARAMS.FAVORITE].stringValue
    }
    init() {
        print(id)
    }
    
//    init(id : String, title :String, amount: String, file_url: String,user_id:String,email:String,username :String,phone:String,type:String,categorie:String,photo:String,description:String ) {
//        self.id = id
//        self.title = title
//        self.amount = amount
//        self.file_url = file_url
//        self.user_id = user_id
//        self.email = email
//        self.username = username
//        self.phone = phone
//        self.type = type
//        self.categorie = categorie
//        self.photo = photo
//        self.description = description
//    }
}

class ContentModel1 {
    
    var id =  ""
    var title =  ""
    var amount =  ""
    var file_url =  ""
    var user_id =  ""
    var email =  ""
    var username =  ""
    var phone =  ""
    var type =  ""
    var categorie =  ""
    var photo =  ""
    var description = ""
    var favorite = ""
    var categorie_id = ""
    
    init(_ content : JSON ) {
        
        self.id = content[PARAMS.ID].stringValue
        self.title = content[PARAMS.TITLE].stringValue
        self.amount = content[PARAMS.AMOUNT].stringValue
        self.file_url = content[PARAMS.FILE_URL].stringValue
        self.user_id = content[PARAMS.USERID].stringValue
        self.email = content[PARAMS.EMAIL].stringValue
        self.username = content[PARAMS.USERNAME].stringValue
        self.phone = content[PARAMS.PHONE].stringValue
        self.type = content[PARAMS.TYPE].stringValue
        self.categorie = content[PARAMS.CATEGORIE].stringValue
        self.photo = content[PARAMS.PHOTO].stringValue
        self.description = content[PARAMS.DESCTIPTION].stringValue
        self.favorite = content[PARAMS.FAVORITE].stringValue
        self.categorie_id = content[PARAMS.CATEGORIE_ID].stringValue
    }
    init() {
        print(id)
    }
}
