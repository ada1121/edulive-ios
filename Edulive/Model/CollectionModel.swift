//
//  MainCategoriesModel.swift
//  Edulive
//
//  Created by Mac on 2/16/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import SwiftyJSON

class CollectionModel{
    var id = ""
    var photo = ""
    var subName = ""
 
    init(_ jsondata : JSON) {
        self.id = jsondata[PARAMS.ID].stringValue
        self.photo = jsondata[PARAMS.IMAGE].stringValue
        self.subName = jsondata[PARAMS.TITLE].stringValue
    }
}

class TeacherModel{
    
    var id = 0
    var email = ""
    var username = ""
    var phone = ""
    var type = ""
    var categorie = ""
    var photo = ""
    
    init(jsondata : JSON) {
        self.id = jsondata[PARAMS.ID].intValue
        self.email = jsondata[PARAMS.EMAIL].stringValue
        self.username = jsondata[PARAMS.USERNAME].stringValue
        self.phone = jsondata[PARAMS.PHONE].stringValue
        self.type = jsondata[PARAMS.TYPE].stringValue
        self.categorie = jsondata[PARAMS.CATEGORIE].stringValue
        self.photo = jsondata[PARAMS.PHOTO].stringValue
    }
}
class CategoriesModel{
    var id = ""
    var title = ""
    init(_ jsondata : JSON){
        self.id = jsondata[PARAMS.ID].stringValue
        self.title = jsondata[PARAMS.TITLE].stringValue
    }
}
