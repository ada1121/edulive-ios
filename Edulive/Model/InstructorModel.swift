//
//  InstructorModel.swift
//  Edulive
//
//  Created by Mac on 3/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//



import Foundation
import SwiftyJSON

class InstructorModel {
    
    var id =  ""
    var title =  ""
    var amount =  ""
    var file_url =  ""
    var user_id =  ""
    var email =  ""
    var username =  ""
    var phone =  ""
    var type =  ""
    var categorie =  ""
    var photo =  ""
    var description = ""
    var paid_student = 0
    var total_student = 0
    var total_favorite = 0
    
    init(_ content : JSON ) {
        
        self.id = content[PARAMS.ID].stringValue
        self.title = content[PARAMS.TITLE].stringValue
        self.description = content[PARAMS.DESCTIPTION].stringValue
        self.amount = content[PARAMS.AMOUNT].stringValue
        self.file_url = content[PARAMS.FILE_URL].stringValue
        self.paid_student = content["paid_student"].intValue
        self.user_id = content[PARAMS.USERID].stringValue
        self.email = content[PARAMS.EMAIL].stringValue
        self.username = content[PARAMS.USERNAME].stringValue
        self.phone = content[PARAMS.PHONE].stringValue
        self.type = content[PARAMS.TYPE].stringValue
        self.categorie = content[PARAMS.CATEGORIE].stringValue
        self.photo = content[PARAMS.PHOTO].stringValue
        self.total_student = content["total_student"].intValue
        self.total_favorite = content["total_favorite"].intValue
    }
    init() {
        print(id)
    }
    
//    init(id : String, title :String, amount: String, file_url: String,user_id:String,email:String,username :String,phone:String,type:String,categorie:String,photo:String,description:String ) {
//        self.id = id
//        self.title = title
//        self.amount = amount
//        self.file_url = file_url
//        self.user_id = user_id
//        self.email = email
//        self.username = username
//        self.phone = phone
//        self.type = type
//        self.categorie = categorie
//        self.photo = photo
//        self.description = description
//    }
}
