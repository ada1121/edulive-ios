
import Foundation

class IntroModel{
    
    var imgBack : String = "camera"
    var title : String = "Welcome to Everave"
    var lbltop : String = "Everave is an ap showing"
    var lblbottom : String = "like parties at your areas"
    var lblbtn : String = "Next"
    
    init(imgBack : String, title :String, lbltop :String, lblbottom :String , lblbtn : String) {
        
        self.imgBack = imgBack
        self.title = title
        self.lbltop = lbltop
        self.lblbottom = lblbottom
        self.lblbtn = lblbtn
    }
}
