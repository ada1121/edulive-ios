//
//  ApiManager.swift
//  PortTrucker
//
//  Created by PSJ on 9/12/19.
//  Copyright © 2019 LiMing. All rights reserved.
//

import Alamofire
import Foundation
import SwiftyJSON
import UIKit

// ************************************************************************//
                                // Food Tracking //
// ************************************************************************//

let API = "https://eduintel.us/api/"
//let API = "http://192.168.0.190:8080/api/"

let LOGIN = API + "signin"
let SIGNUP = API + "signup"
let FORGOT = API + "forgot"
let GETRECOMMENDEDTEACHER = API + "getRecommend"
let GETCATEGORIES = API + "getCategories"
let GETCONTENTS = API + "getContents"
let GETPUBLICCONTENTS = API + "getPublicContents"
let UPLOADCONTENT = API + "uploadContent"
let GETUSERS = API + "getUsers"
let GETCONTENSFAVORITES = API + "getContentsFavorite"
let GETMYCONTENTS = API + "getMyContents"
let GETMYSTUDENTS = API + "getMyStudents"
let GETMYDETAIL = API + "getMyDetails"
let REMOVECONTENT = API + "removeContent"
let UPDATECONTENT = API + "updateContent"
let GETPAID = API + "getPaid"
let GETALLCONTENTS = API + "getAllContents"
let GETMYALLCONTENTS = API + "getMyAllContents"
let FAVORITE = API + "favorite"
let EDITPROFILE = API + "editProfile"
let GETPURCHASEDLESSONS = API + "getPaidContents"

let FIRST_REQUEST = "https://api.mch.weixin.qq.com/pay/unifiedorder"

struct PARAMS {
    // ************************************************************************//
                                    // FoodTracking //
    // ************************************************************************//
    static let STATUS = "result_code"
    static let TRUE = "0"
    static let USERINFO = "user_info"
    static let EMAIL = "email"
    static let USERNAME = "username"
    static let PASSWORD = "password"
    static let ID = "id"
    static let TITLE = "title"
    static let CATEGORIE = "categorie"
    static let USERID = "user_id"
    static let TYPE = "type"
    static let PHONE = "phone"
    static let PHOTO = "photo"
    static let AMOUNT = "amount"
    static let CATEGORIE_ID = "categorie_id"
    static let CONTENT_LIST = "content_list"
    static let FILE_URL = "file_url"
    static let CATEGORY_LIST = "category_list"
    static let DESCTIPTION = "description"
    static let IMAGE = "image"
    static let FILE = "file"
    static let TEACHER = "teacher"
    static let STUDENT = "student"
    static let USER_LIST = "user_list"
    static let FAVORITE_LIST = "favorite_list"
    static let RECOMMENDED = "recomended"
    static let STUDENTS_LIST = "student_list"
    static let USER_INFO = "user_info"
    static let CONTENT_ID = "content_id"
    static let FAVORITE = "favorite"
    static let SOCIAL_LOGIN = "social_login"
}


class ApiManager {
    
    class func login(useremail : String, password : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["email" : useremail, "password" : password ] as [String : Any]
        Alamofire.request(LOGIN, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    print(dict)
                    let status = dict[PARAMS.STATUS].stringValue // 0,1,2
                    let user_info = dict[PARAMS.USERINFO].object
                    let userdata = JSON(user_info)
                    if status == PARAMS.TRUE {
                        completion(true, userdata)
                    } else {
                        completion(false, status)
                    }
            }
        }
    }
     
    class func signup(email : String,  phone : String, username : String ,password : String,type : String,categorie : String, images : [String], completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
          let requestURL = SIGNUP
                 Alamofire.upload(
                     multipartFormData: { multipartFormData in
                        
                        for one in images {
                            multipartFormData.append(URL(fileURLWithPath:one),withName:PARAMS.PHOTO)
                        }
                        multipartFormData.append( username.data(using:String.Encoding.utf8)!, withName: PARAMS.USERNAME)
                        multipartFormData.append( email.data(using:String.Encoding.utf8)!, withName: PARAMS.EMAIL)
                        multipartFormData.append( phone.data(using:String.Encoding.utf8)!, withName: PARAMS.PHONE)
                        multipartFormData.append( password.data(using:String.Encoding.utf8)!, withName: PARAMS.PASSWORD)
                        multipartFormData.append( type.data(using:String.Encoding.utf8)!, withName: PARAMS.TYPE)
                        multipartFormData.append( categorie.data(using:String.Encoding.utf8)!, withName: PARAMS.CATEGORIE)

                 },
                     to: requestURL,
                     encodingCompletion: { encodingResult in
                         switch encodingResult {
                             case .success(let upload, _, _):
                                 upload.responseJSON { response in
                                     switch response.result {
                                         case .failure: completion(false, nil)
                                         case .success(let data):
                                             let dict = JSON(data)
                                             print("")
                                             let status = dict[PARAMS.STATUS].stringValue
                                             if status == PARAMS.TRUE {
                                                 completion(true, status)
                                             } else if status == "2" {
                                                 completion(true, status)
                                             } else{
                                                completion(false, status)
                                            }
                                         }
                                 }
                         case .failure( _):
                                 print("")
                                 completion(false, nil)
                             }
                 })
    }

    class func forgot(email : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
            let params = [PARAMS.EMAIL: email] as [String : Any]
            Alamofire.request( FORGOT, method:.post, parameters:params)
                .responseJSON { response in
                    switch response.result {
                        case .failure:
                            completion(false, nil)
                        case .success(let data):
                            let dict = JSON(data)
                            let status = dict[PARAMS.STATUS].stringValue
                            let pincode = dict["pincode"].stringValue
                            if status == PARAMS.TRUE {
                                completion(true, pincode)
                            } else {
                                completion(false, status)
                            }
                        }
            }
        }
    class func getRecommend(completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
               
       Alamofire.request( GETRECOMMENDEDTEACHER, method:.post)
           .responseJSON { response in
               switch response.result {
                   case .failure:
                       completion(false, nil)
                   case .success(let data):
                       let dict = JSON(data)
                       let status = dict[PARAMS.STATUS].stringValue
                       let recommendedTeachers = dict["recomended_teachers"].arrayObject
                       if status == PARAMS.TRUE {
                           completion(true, recommendedTeachers)
                       } else {
                           completion(false, status)
                       }
                   }
       }
   }
    
    class func getContent(categorie_id : Int, amount : Int,user_id: Int,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = [PARAMS.CATEGORIE_ID : categorie_id,PARAMS.AMOUNT : amount,PARAMS.USERID : user_id] as [String : Any]
        Alamofire.request( GETCONTENTS,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    print(dict)
                    let status = dict[PARAMS.STATUS].stringValue // 0,1,2
                    let content_listJSON = dict[PARAMS.CONTENT_LIST].arrayObject
                    let content_list = JSON(content_listJSON as Any)
                    if status == PARAMS.TRUE {
                        //print("this is fooddata =====",fooddata as Any)
                        completion(true, content_list)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    
    class func getPublicContents(amount : Int,user_id: Int,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = [PARAMS.AMOUNT : amount,PARAMS.USERID : user_id] as [String : Any]
        Alamofire.request( GETPUBLICCONTENTS,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    print(dict)
                    let status = dict[PARAMS.STATUS].stringValue // 0,1,2
                    let content_listJSON = dict[PARAMS.CONTENT_LIST].arrayObject
                    let content_list = JSON(content_listJSON as Any)
                    if status == PARAMS.TRUE {
                        //print("this is fooddata =====",fooddata as Any)
                        completion(true, content_list)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    class func getCategories(completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
                  
        Alamofire.request( GETCATEGORIES, method:.post)
              .responseJSON { response in
                  switch response.result {
                      case .failure:
                          completion(false, nil)
                      case .success(let data):
                          let dict = JSON(data)
                          let status = dict[PARAMS.STATUS].stringValue
                          let categories_list = dict[PARAMS.CATEGORY_LIST].arrayObject
                          if status == PARAMS.TRUE {
                              completion(true, categories_list)
                          } else {
                              completion(false, status)
                          }
                      }
                }
      }

    class func uploadContent( user_id : String,  categorie_id : String, title : String ,amount : String,description : String, file : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {

        let requestURL = UPLOADCONTENT
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append( user_id.data(using:String.Encoding.utf8)!, withName: PARAMS.USERID)
                multipartFormData.append( categorie_id.data(using:String.Encoding.utf8)!, withName: PARAMS.CATEGORIE_ID)
                multipartFormData.append( title.data(using:String.Encoding.utf8)!, withName: PARAMS.TITLE)
                multipartFormData.append( amount.data(using:String.Encoding.utf8)!, withName: PARAMS.AMOUNT)
                multipartFormData.append( description.data(using:String.Encoding.utf8)!, withName: PARAMS.DESCTIPTION)
                if file != ""{
                    
                    var dir: URL!
                    do {
                        dir = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                    } catch{
                        print("error re-creating url")
                    }

                    
                    let pdfURL = dir.appendingPathComponent(file)
                    multipartFormData.append(pdfURL,withName:PARAMS.FILE)
                }
                else{
                    multipartFormData.append( file.data(using:String.Encoding.utf8)!, withName: PARAMS.FILE)
                }
            },
            to: requestURL,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                    case .success(let upload, _, _):
                    upload.responseJSON { response in
                        switch response.result {
                            case .failure: completion(false, nil)
                            case .success(let data):
                            let dict = JSON(data)
                            //print("")
                            let status = dict[PARAMS.STATUS].stringValue
                            if status == PARAMS.TRUE {
                                completion(true, status)
                            } else if status == "1"  {
                                completion(true, status)
                            }
                            else if status == "2" {
                                completion(true, status)
                            }
                            else{
                                completion(false, status)
                            }
                        }
                    }
                    case .failure(let encodingError):
                    print(encodingError)
                    completion(false, nil)
                }
            }
        )
    }
    
    class func updateContent( content_id : String, title : String ,amount : String,description : String, file : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {

        let requestURL = UPDATECONTENT
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append( content_id.data(using:String.Encoding.utf8)!, withName: PARAMS.CONTENT_ID)
                multipartFormData.append( title.data(using:String.Encoding.utf8)!, withName: PARAMS.TITLE)
                multipartFormData.append( amount.data(using:String.Encoding.utf8)!, withName: PARAMS.AMOUNT)
                multipartFormData.append( description.data(using:String.Encoding.utf8)!, withName: PARAMS.DESCTIPTION)
                if file != ""{
                    
                    var dir: URL!
                    do {
                        dir = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                    } catch{
                        print("error re-creating url")
                    }

                    
                    let pdfURL = dir.appendingPathComponent(file)
                    multipartFormData.append(pdfURL,withName:PARAMS.FILE)
                }
                else{
                    multipartFormData.append( file.data(using:String.Encoding.utf8)!, withName: PARAMS.FILE)
                }
            },
            to: requestURL,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                    case .success(let upload, _, _):
                    upload.responseJSON { response in
                        switch response.result {
                            case .failure: completion(false, nil)
                            case .success(let data):
                            let dict = JSON(data)
                            //print("")
                            let status = dict[PARAMS.STATUS].stringValue
                            if status == PARAMS.TRUE {
                                completion(true, status)
                            } else if status == "1"  {
                                completion(true, status)
                            }
                            else if status == "2" {
                                completion(true, status)
                            }
                            else{
                                completion(false, status)
                            }
                        }
                    }
                    case .failure(let encodingError):
                    print(encodingError)
                    completion(false, nil)
                }
            }
        )
    }
    
    class func getUsers(completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
                
      Alamofire.request( GETUSERS, method:.post)
            .responseJSON { response in
                switch response.result {
                    case .failure:
                        completion(false, nil)
                    case .success(let data):
                        let dict = JSON(data)
                        let status = dict[PARAMS.STATUS].stringValue
                        let user_list = dict[PARAMS.USER_LIST].arrayObject
                        if status == PARAMS.TRUE {
                            completion(true, user_list)
                        } else {
                            completion(false, status)
                        }
                    }
              }
    }
    class func getcontentFavorites(user_id : Int, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = [PARAMS.USERID: user_id] as [String : Any]
        Alamofire.request( GETCONTENSFAVORITES, method:.post, parameters:params)
            .responseJSON { response in
                switch response.result {
                    case .failure:
                        completion(false, nil)
                    case .success(let data):
                        let dict = JSON(data)
                        let status = dict[PARAMS.STATUS].stringValue
                        let favorite_list = dict[PARAMS.FAVORITE_LIST].arrayObject
                        if status == PARAMS.TRUE {
                            completion(true, favorite_list)
                        } else {
                            completion(false, status)
                        }
                    }
        }
    }
    
    class func getMyContents(user_id : Int,categorie_id: Int, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = [PARAMS.USERID: user_id,PARAMS.CATEGORIE_ID : categorie_id] as [String : Any]
        Alamofire.request( GETMYCONTENTS, method:.post, parameters:params)
            .responseJSON { response in
                switch response.result {
                    case .failure:
                        completion(false, nil)
                    case .success(let data):
                        let dict = JSON(data)
                        let status = dict[PARAMS.STATUS].stringValue
                        let content_list = dict[PARAMS.CONTENT_LIST].arrayObject
                        if status == PARAMS.TRUE {
                            completion(true, content_list)
                        } else {
                            completion(false, status)
                        }
                    }
        }
    }
    
    class func getMyStudents(user_id : Int, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = [PARAMS.USERID: user_id] as [String : Any]
        Alamofire.request( GETMYSTUDENTS, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                let status = dict[PARAMS.STATUS].stringValue
                let student_List = dict[PARAMS.STUDENTS_LIST].arrayObject
                if status == PARAMS.TRUE {
                    completion(true, student_List)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func getMyDetail(email : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = [PARAMS.EMAIL: email] as [String : Any]
        Alamofire.request( GETMYDETAIL, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                let status = dict[PARAMS.STATUS].stringValue
                let userinfo = dict[PARAMS.USER_INFO].object
                let jsonuserinfo = JSON(userinfo)
                if status == PARAMS.TRUE {
                    completion(true, jsonuserinfo)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func removeMyContent(content_id : Int, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = [PARAMS.CONTENT_ID: content_id] as [String : Any]
        Alamofire.request( REMOVECONTENT, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                let status = dict[PARAMS.STATUS].stringValue
                
                if status == PARAMS.TRUE {
                    completion(true, status)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func getPaid(user_id : Int,content_id: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = [PARAMS.USERID: user_id,PARAMS.CONTENT_ID : content_id] as [String : Any]
        Alamofire.request( GETPAID, method:.post, parameters:params)
            .responseJSON { response in
                switch response.result {
                    case .failure:
                        completion(false, nil)
                    case .success(let data):
                        let dict = JSON(data)
                        let status = dict[PARAMS.STATUS].stringValue
                        
                        if status == PARAMS.TRUE {
                            completion(true, status)
                        } else {
                            completion(false, status)
                        }
                    }
        }
    }
    
    class func getAllContents(completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
                
      Alamofire.request( GETALLCONTENTS, method:.post)
            .responseJSON { response in
                switch response.result {
                    case .failure:
                        completion(false, nil)
                    case .success(let data):
                        let dict = JSON(data)
                        let status = dict[PARAMS.STATUS].stringValue
                        let content_List = dict[PARAMS.CONTENT_LIST].arrayObject
                        if status == PARAMS.TRUE {
                            completion(true, content_List)
                        } else {
                            completion(false, status)
                        }
                    }
              }
    }
    
    class func getMyAllContents(user_id : Int, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = [PARAMS.USERID: user_id] as [String : Any]
        Alamofire.request( GETMYALLCONTENTS, method:.post, parameters:params)
            .responseJSON { response in
                switch response.result {
                    case .failure:
                        completion(false, nil)
                    case .success(let data):
                        let dict = JSON(data)
                        let status = dict[PARAMS.STATUS].stringValue
                        let content_list = dict[PARAMS.CONTENT_LIST].arrayObject
                        if status == PARAMS.TRUE {
                            completion(true, content_list)
                        } else {
                            completion(false, status)
                        }
                    }
        }
    }
    
    class func favorite(user_id : Int,content_id: String,status : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = [PARAMS.USERID: user_id,PARAMS.CONTENT_ID : content_id,"status" : status] as [String : Any]
           Alamofire.request( FAVORITE, method:.post, parameters:params)
               .responseJSON { response in
                   switch response.result {
                       case .failure:
                           completion(false, nil)
                       case .success(let data):
                           let dict = JSON(data)
                           let status = dict[PARAMS.STATUS].stringValue
                           
                           if status == PARAMS.TRUE {
                               completion(true, status)
                           } else {
                               completion(false, status)
                           }
                       }
           }
       }
    class func editProfile(email : String,  phone : String, username : String ,password : String,categorie: String, images : [String],user_id : Int, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
          let requestURL = EDITPROFILE
                 Alamofire.upload(
                     multipartFormData: { multipartFormData in
                        if images.count != 0{
                            for one in images {
                                multipartFormData.append(URL(fileURLWithPath:one),withName:PARAMS.PHOTO)
                            }
                        }
                        multipartFormData.append( username.data(using:String.Encoding.utf8)!, withName: PARAMS.USERNAME)
                        multipartFormData.append( email.data(using:String.Encoding.utf8)!, withName: PARAMS.EMAIL)
                        multipartFormData.append( phone.data(using:String.Encoding.utf8)!, withName: PARAMS.PHONE)
                        multipartFormData.append( password.data(using:String.Encoding.utf8)!, withName: PARAMS.PASSWORD)
                        multipartFormData.append( categorie.data(using:String.Encoding.utf8)!, withName: PARAMS.CATEGORIE)
                        multipartFormData.append( "\(user_id)".data(using:String.Encoding.utf8)!, withName: PARAMS.USERID)

                 },
                     to: requestURL,
                     encodingCompletion: { encodingResult in
                         switch encodingResult {
                             case .success(let upload, _, _):
                                 upload.responseJSON { response in
                                     switch response.result {
                                         case .failure: completion(false, nil)
                                         case .success(let data):
                                             let dict = JSON(data)
                                            
                                             let status = dict[PARAMS.STATUS].stringValue
                                             if status == PARAMS.TRUE {
                                                 completion(true, status)
                                             } else if status == "1" {
                                                 completion(true, status)
                                             } else{
                                                completion(false, status)
                                            }
                                         }
                                 }
                         case .failure( _):
                                 print("")
                                 completion(false, nil)
                             }
                 })
    }
    
    class func getPurchasedLessons(user_id : Int, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = [PARAMS.USERID: user_id] as [String : Any]
        Alamofire.request( GETPURCHASEDLESSONS, method:.post, parameters:params)
            .responseJSON { response in
                switch response.result {
                    case .failure:
                        completion(false, nil)
                    case .success(let data):
                        let dict = JSON(data)
                        let status = dict[PARAMS.STATUS].stringValue
                        let purchased = dict[PARAMS.CONTENT_LIST].arrayObject
                        if status == PARAMS.TRUE {
                            completion(true, purchased)
                        } else {
                            completion(false, status)
                        }
                    }
        }
    }
}
