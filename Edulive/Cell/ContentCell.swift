//
//  ContentCell.swift
//  Edulive
//
//  Created by Mac on 2/19/20.
//  Copyright © 2020 Mac. All rights reserved.
//


import Foundation
import UIKit
import SwipeCellKit
import Kingfisher

class ContentCell : SwipeCollectionViewCell {
    
    @IBOutlet var teacherAvatar: UIImageView!
    @IBOutlet var lessonTitle: UILabel!
    @IBOutlet var teacherName: UILabel!
    @IBOutlet var price: UILabel!
    
    @IBOutlet weak var avartarBtn: UIButton!
    @IBOutlet weak var titleBtn: UIButton!
    @IBOutlet weak var teacherBtn: UIButton!
    @IBOutlet weak var favoriteBtn: UIButton!
    
    @IBOutlet weak var imv_unhard: UIImageView!
    @IBOutlet weak var imv_hard: UIImageView!
    
    var entity: ContentModel!{
        
        didSet{
            let url = URL(string: entity.photo)
            teacherAvatar.kf.setImage(with: url,placeholder: UIImage(named: "account_photo"))
            lessonTitle.text = entity.title
            teacherName.text = entity.username
            price.text = "$" + " " + entity.amount
            if user!.type == PARAMS.STUDENT{
                if entity.favorite == "yes"{
                   self.imv_unhard.isHidden = true
                   self.imv_hard.isHidden = false
               }
               else{
                   self.imv_unhard.isHidden = false
                   self.imv_hard.isHidden = true
               }
            }
            else{
                self.imv_unhard.isHidden = true
                self.imv_hard.isHidden = true
            }
        }
    }
}

class ContentCell1 : SwipeCollectionViewCell {
    
    @IBOutlet var teacherAvatar: UIImageView!
    @IBOutlet var lessonTitle: UILabel!
    @IBOutlet var teacherName: UILabel!
    @IBOutlet var price: UILabel!
    
//    @IBOutlet weak var avartarBtn: UIButton!
//    @IBOutlet weak var titleBtn: UIButton!
//    @IBOutlet weak var teacherBtn: UIButton!
//    @IBOutlet weak var favoriteBtn: UIButton!
    
    @IBOutlet weak var imv_unhard: UIImageView!
    @IBOutlet weak var imv_hard: UIImageView!
    
    var entity: ContentModel1!{
        
        didSet{
            let url = URL(string: entity.photo)
            teacherAvatar.kf.setImage(with: url,placeholder: UIImage(named: "account_photo"))
            lessonTitle.text = entity.title
            teacherName.text = entity.username
            price.text = "$" + " " + entity.amount
            if user!.type == PARAMS.STUDENT{
                if entity.favorite == "yes"{
                   self.imv_unhard.isHidden = true
                   self.imv_hard.isHidden = false
               }
               else{
                   self.imv_unhard.isHidden = false
                   self.imv_hard.isHidden = true
               }
            }
            else{
                self.imv_unhard.isHidden = true
                self.imv_hard.isHidden = true
            }
        }
    }
}

class TeacherCell : SwipeCollectionViewCell {
    
    @IBOutlet var teacherAvatar: UIImageView!
    @IBOutlet var lessonTitle: UILabel!
    @IBOutlet var teacherName: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var lbl_paidStudent: UILabel!
     
    var entity: LessonModel!{
        
        didSet{
            let url = URL(string: user!.photo ?? "") // must be chaged
            teacherAvatar.kf.setImage(with: url,placeholder: UIImage(named: "account_photo"))
            lessonTitle.text = entity.title
            teacherName.text = user!.username
            price.text = "$" + " " + entity.amount
            self.lbl_paidStudent.text = "\(entity.paid_student)"
        }
    }
}

class StudentCell : SwipeCollectionViewCell {
    
    @IBOutlet var imv_student : UIImageView!
    @IBOutlet var lbl_email: UILabel!
    @IBOutlet var lbl_phone: UILabel!
    @IBOutlet var lbl_username: UILabel!
     
    var entity: StudentModel!{
        
        didSet{
            let url = URL(string: entity.photo) // must be chaged
            imv_student.kf.setImage(with: url,placeholder: UIImage(named: "man"))
            lbl_email.text = entity.email
            lbl_phone.text = entity.phone
            lbl_username.text = entity.username
        }
    }
}

class InstructorCell : SwipeCollectionViewCell {
    
    @IBOutlet var teacherAvatar: UIImageView!
    @IBOutlet var lessonTitle: UILabel!
    @IBOutlet var subjectName: UILabel!
    @IBOutlet var price: UILabel!
     
    @IBOutlet weak var lessonStudent: UILabel!
    var entity: InstructorModel!{
        
        didSet{
            let url = URL(string: entity.photo) // must be chaged
            teacherAvatar.kf.setImage(with: url,placeholder: UIImage(named: "account_photo"))
            lessonTitle.text = entity.title
            subjectName.text = entity.categorie
            price.text = "$" + " " + entity.amount
            lessonStudent.text = "\(entity.paid_student)"
        }
    }
}

class FavoriteCell : SwipeCollectionViewCell {
    
    @IBOutlet var teacherAvatar: UIImageView!
    @IBOutlet var lessonTitle: UILabel!
    @IBOutlet var teacherName: UILabel!
    @IBOutlet var price: UILabel!
    
    @IBOutlet weak var imv_unhard: UIImageView!
    @IBOutlet weak var imv_hard: UIImageView!
    @IBOutlet weak var favoriteBtn: UIButton!
    
    var entity: ContentModel!{
        
        didSet{
            let url = URL(string: entity.photo)
            teacherAvatar.kf.setImage(with: url,placeholder: UIImage(named: "account_photo"))
            lessonTitle.text = entity.title
            teacherName.text = entity.username
            price.text = "$" + " " + entity.amount
            if entity.favorite == "yes"{
                self.imv_hard.isHidden = false
                self.imv_unhard.isHidden = true
            }
            else if entity.favorite == "no"{
                self.imv_hard.isHidden = true
                self.imv_unhard.isHidden = false
            }
            else{
                self.imv_hard.isHidden = false
                self.imv_unhard.isHidden = true
            }
        }
    }
}

