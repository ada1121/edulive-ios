//
//  AllContensModel.swift
//  Edulive
//
//  Created by Mac on 3/7/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class AllContentsCell : UICollectionViewCell {
    
    @IBOutlet var randomImage: UIImageView!
    @IBOutlet var lessonTitle: UILabel!
    @IBOutlet var price: UILabel!
    
    var entity: ContentModel!{
           
           didSet{
                randomImage.image = UIImage.init(named: "\(entity.id.toInt()! % 9)" + ".png")
               lessonTitle.text = entity.title
               price.text = "$" + " " + entity.amount
           }
   }
    override var isSelected: Bool {
      didSet {
        self.randomImage.alpha = isSelected ? 0.5 : 10.0
        self.price.textColor = isSelected ? UIColor.black.withAlphaComponent(0.8) : UIColor.black
        self.lessonTitle.textColor = isSelected ? UIColor.black.withAlphaComponent(0.8) : UIColor.black
      }
    }
}

