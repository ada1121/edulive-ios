//
//  myLessonCell.swift
//  Edulive
//
//  Created by Mac on 3/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import UIKit
import SwipeCellKit
import Kingfisher

class myLessonCell : UICollectionViewCell {
    
    @IBOutlet var teacherAvatar: UIImageView!
    @IBOutlet var lessonTitle: UILabel!
    
    @IBOutlet weak var sujectName: UILabel!
    
    var entity: ContentModel1!{
        
        didSet{
            
            lessonTitle.text = entity.title
            teacherAvatar.image = UIImage.init(named: "\(entity.id.toInt()! % 9)" + ".png")
            if user!.type == PARAMS.TEACHER{
                if let name = getSubNameFromId(entity.categorie_id){
                    sujectName.text = name
                }
            }
        }
    }
    
    func getSubNameFromId(_ id: String) -> String? {
        var subname : String?
        for one in subDataSource{
            if one.id == id{
                subname = one.subName
                break
            }
        }
        return subname
    }
}
