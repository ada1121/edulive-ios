//
//  SettingCell.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/18/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import Foundation
import UIKit

class SettingCell: UITableViewCell {
    
    
    @IBOutlet weak var setting_lbl: UILabel!
    
    @IBOutlet weak var switch_btn: UISwitch!
    @IBOutlet weak var rightArrow: UIImageView!
    @IBOutlet weak var languageCaption: UILabel!
    
    var entity : SettingModel!{
        
        didSet{
            setting_lbl.text = entity.settingCaption
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
