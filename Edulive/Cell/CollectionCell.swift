//
//  CollectionCell.swift
//  Edulive
//
//  Created by Mac on 2/16/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import UIKit
import SwipeCellKit
import Kingfisher

class CollectionCell: UICollectionViewCell {
    
    @IBOutlet var photo: UIImageView!
    @IBOutlet var subName: UILabel!
    
    var entity:CollectionModel!{
        
        didSet{
            let url = URL(string: entity.photo)
            photo.kf.setImage(with: url,placeholder: UIImage(named: "appLogo"))
            subName.text = entity.subName
            self.photo.alpha = 0.75
        }
    }
}

class TeacherCollectionCell: UICollectionViewCell {
    
    @IBOutlet var photo: UIImageView!
    @IBOutlet var subName: UILabel!
    @IBOutlet weak var subjectName: UILabel!
    
    var entity: TeacherModel!{
        
        didSet{
            let url = URL(string: entity.photo)
            photo.kf.setImage(with: url,placeholder: UIImage(named: "account_photo"))
            subName.text = entity.username
            subjectName.text = entity.categorie
        }
    }
}
