//
//  KeyCenter.swift
//  OpenLive
//
//  Created by GongYuhua on 6/25/16.
//  Copyright © 2016 Agora. All rights reserved.

//
struct KeyCenter {
    static let AppId: String = "44304034b8034e8a9578e6b94a1aba02"
    
    // assign token to nil if you have not enabled app certificate
    static var Token: String?
}
