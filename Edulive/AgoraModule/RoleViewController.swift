//
//  RoleViewController.swift
//  OpenLive
//
//  Created by CavanSu on 2019/8/28.
//  Copyright © 2019 Agora. All rights reserved.
//

import UIKit
import AgoraRtcEngineKit

protocol RoleVCDelegate: NSObjectProtocol {
    func roleVC(_ vc: RoleViewController, didSelect role: AgoraClientRole)
}

class RoleViewController: BaseVC1 {

    @IBOutlet weak var teacherView: UIView!
    @IBOutlet weak var studentView: UIView!
    
    @IBOutlet weak var teacherCenterY: NSLayoutConstraint!
    @IBOutlet weak var studentCenterY: NSLayoutConstraint!
    weak var delegate: RoleVCDelegate?
    
    @IBOutlet weak var teacherCaption: UILabel!
    @IBOutlet weak var broadcaster: UIImageView!
    
    @IBOutlet weak var lbl_content: UILabel!
    
    @IBOutlet weak var bottomCaption: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        if user!.type == PARAMS.TEACHER{
            let welcome = NSLocalizedString("Welcome", comment: "")
            let broadcasting = NSLocalizedString("Start Broadcasting", comment: "")
            teacherCaption.text = welcome
            bottomCaption.text = broadcasting
            self.teacherView.isHidden = false
            self.studentView.isHidden = true
            //self.teacherCenterY.constant = UIScreen.main.bounds.height / 2
        }
        else{
            
            let conversation = NSLocalizedString("Conversation", comment: "")
            let start_conversation = NSLocalizedString("Start Conversation", comment: "")
            teacherCaption.text = conversation
            bottomCaption.text = start_conversation
            
            broadcaster.image = UIImage.init(named: "teacher_student.png")
            self.teacherView.isHidden = false
            self.studentView.isHidden = false
            //self.studentCenterY.constant = UIScreen.main.bounds.height / 2
        }
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//       if user!.type == PARAMS.TEACHER{
//           //self.teacherCenterY.constant = UIScreen.main.bounds.height / 2 - 75
//       }
//       else{
//           //self.studentCenterY.constant = UIScreen.main.bounds.height / 2
//       }
//    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let segueId = segue.identifier,
            segueId.count > 0 else {
            return
        }
        
        switch segueId {
        case "roleToLive":
            guard let mainVC = navigationController?.viewControllers.first as? MainViewController,
                let liveVC = segue.destination as? LiveRoomViewController else {
                return
            }
            
            liveVC.dataSource = mainVC
        default:
            break
        }
    }
    
    func selectedRoleToLive(role: AgoraClientRole) {
        delegate?.roleVC(self, didSelect: role)
        performSegue(withIdentifier: "roleToLive", sender: nil)
    }
    
    @IBAction func doBroadcasterTap(_ sender: UITapGestureRecognizer) {
        selectedRoleToLive(role: .broadcaster)
    }
    
    @IBAction func doAudienceTap(_ sender: UITapGestureRecognizer) {
        selectedRoleToLive(role: .audience)
    }
    @IBAction func gotoBacke(_ sender: Any) {
        var navigationController1: UINavigationController!
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
       navigationController1 = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "MainViewController"))
        navigationController1!.modalPresentationStyle = .fullScreen
        self.present(navigationController1!, animated: true, completion: nil)
    }
}
